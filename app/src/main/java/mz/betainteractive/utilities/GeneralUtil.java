package mz.betainteractive.utilities;

import android.widget.DatePicker;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by paul on 4/13/17.
 */
public class GeneralUtil {

    public static Date getDate(DatePicker datePicker){
        return new Date(datePicker.getCalendarView().getDate());
    }

    public static int getAge(Date dobDate){
        Calendar now = Calendar.getInstance();
        Calendar dob = Calendar.getInstance();
        dob.setTime(dobDate);

        int age = now.get(Calendar.YEAR)-dob.get(Calendar.YEAR) + (now.get(Calendar.DAY_OF_YEAR)<dob.get(Calendar.DAY_OF_YEAR) ? -1 : 0);

        return age;
    }

    public static int getAge(Date dobDate, Date endDate){
        Calendar end = Calendar.getInstance();
        Calendar dob = Calendar.getInstance();
        end.setTime(endDate);
        dob.setTime(dobDate);

        int age = end.get(Calendar.YEAR)-dob.get(Calendar.YEAR) + (end.get(Calendar.DAY_OF_YEAR)<dob.get(Calendar.DAY_OF_YEAR) ? -1 : 0);

        return age;
    }

    static Calendar getCalendar(Date date){
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal;
    }

    static Date getDate(int y, int m, int d){
        Calendar cal = Calendar.getInstance();
        cal.set(y, m, d);
        return cal.getTime();
    }

    static Date getDate(int y, int m, int d, int hour, int min, int sec){
        Calendar cal = Calendar.getInstance();
        cal.set(y, m, d, hour, min, sec);
        return cal.getTime();
    }
}
