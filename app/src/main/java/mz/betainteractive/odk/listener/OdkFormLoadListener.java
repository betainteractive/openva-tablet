package mz.betainteractive.odk.listener;

import android.net.Uri;

public interface OdkFormLoadListener {

	void onOdkFormLoadSuccess(Uri contentUri);
	
	void onOdkFormLoadFailure();

}
