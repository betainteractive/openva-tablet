package net.openva.mobile.database;

import android.content.Context;
import android.database.Cursor;

import net.openva.mobile.model.*;

import java.util.ArrayList;
import java.util.List;


/**
 * Generic Queries to retrieve data from the Database
 */
public class Queries {
    public static SyncReport getSyncReportBy(Database database, String whereClause, String[] clauseArgs){
        SyncReport report = null;

        Cursor cursor = database.query(SyncReport.class, whereClause, clauseArgs, null, null, null);

        if (cursor.moveToFirst()){
            report = Converter.cursorToSyncReport(cursor);
        }

        return report;
    }

    public static List<SyncReport> getAllSyncReportBy(Database database, String whereClause, String[] clauseArgs){
        List<SyncReport> list = new ArrayList<>();

        Cursor cursor = database.query(SyncReport.class, whereClause, clauseArgs, null, null, null);

        while (cursor.moveToNext()){
            SyncReport report = Converter.cursorToSyncReport(cursor);
            list.add(report);
        }

        return list;
    }

    public static CollectedData getCollectedDataBy(Database database, String whereClause, String[] clauseArgs){
        CollectedData cd = null;

        Cursor cursor = database.query(CollectedData.class, DatabaseHelper.CollectedData.ALL_COLUMNS,  whereClause, clauseArgs, null, null, null);

        if (cursor.moveToFirst()){
            cd = Converter.cursorToCollectedData(cursor);
        }

        return cd;
    }

    public static List<CollectedData> getAllCollectedDataBy(Database database, String whereClause, String[] clauseArgs){
        List<CollectedData> list = new ArrayList<>();

        Cursor cursor = database.query(CollectedData.class, DatabaseHelper.CollectedData.ALL_COLUMNS, whereClause, clauseArgs, null, null, null);

        while (cursor.moveToNext()){
            CollectedData cd = Converter.cursorToCollectedData(cursor);
            list.add(cd);
        }

        return list;
    }

    public static User getUserBy(Database database, String whereClause, String[] clauseArgs){
        User user = null;

        Cursor cursor = database.query(User.class, whereClause, clauseArgs, null, null, null);

        if (cursor.moveToFirst()){
            user = Converter.cursorToUser(cursor);
        }

        return user;
    }

    public static List<User> getAllUserBy(Database database, String whereClause, String[] clauseArgs){
        List<User> list = new ArrayList<>();

        Cursor cursor = database.query(User.class, whereClause, clauseArgs, null, null, null);

        while (cursor.moveToNext()){
            User user = Converter.cursorToUser(cursor);
            list.add(user);
        }

        return list;
    }

    public static VerbalAutopsyControl getVerbalAutopsyControlBy(Database database, String whereClause, String[] clauseArgs){
        VerbalAutopsyControl vac = null;

        Cursor cursor = database.query(VerbalAutopsyControl.class, whereClause, clauseArgs, null, null, null);

        if (cursor.moveToFirst()){
            vac = Converter.cursorToVerbalAutopsyControl(cursor);
        }

        return vac;
    }

    public static List<VerbalAutopsyControl> getAllVerbalAutopsyControlBy(Database database, String whereClause, String[] clauseArgs){
        List<VerbalAutopsyControl> list = new ArrayList<>();

        Cursor cursor = database.query(VerbalAutopsyControl.class, whereClause, clauseArgs, null, null, null);

        while (cursor.moveToNext()){
            VerbalAutopsyControl vac = Converter.cursorToVerbalAutopsyControl(cursor);
            list.add(vac);
        }

        return list;
    }

    public static List<VerbalAutopsyControl> getAllVerbalAutopsyControlBy(Database database, String whereClause, String[] clauseArgs, int limit){
        List<VerbalAutopsyControl> list = new ArrayList<>();

        int n = 0;
        Cursor cursor = database.query(VerbalAutopsyControl.class, whereClause, clauseArgs, null, null, null);

        while (cursor.moveToNext()){
            VerbalAutopsyControl vac = Converter.cursorToVerbalAutopsyControl(cursor);
            list.add(vac);
            if (limit==++n) break;
        }

        return list;
    }

    public static Individual getIndividualBy(Database database, String whereClause, String[] clauseArgs){
        Individual ind = null;

        Cursor cursor = database.query(Individual.class, whereClause, clauseArgs, null, null, null);

        if (cursor.moveToFirst()){
            ind = Converter.cursorToIndividual(cursor);
        }

        return ind;
    }

    public static List<Individual> getAllIndividualBy(Database database, String whereClause, String[] clauseArgs){
        List<Individual> list = new ArrayList<>();

        Cursor cursor = database.query(Individual.class, whereClause, clauseArgs, null, null, null);

        while (cursor.moveToNext()){
            Individual ind = Converter.cursorToIndividual(cursor);
            list.add(ind);
        }

        return list;
    }

    public static List<Individual> getAllIndividualBy(Database database, String whereClause, String[] clauseArgs, int limit){
        List<Individual> list = new ArrayList<>();

        int n = 0;
        Cursor cursor = database.query(Individual.class, whereClause, clauseArgs, null, null, null);

        while (cursor.moveToNext()){
            Individual ind = Converter.cursorToIndividual(cursor);
            list.add(ind);
            if (limit==++n) break;
        }

        return list;
    }

    public static VerbalAutopsyStats getVerbalAutopsyStatsBy(Database database, String whereClause, String[] clauseArgs){
        VerbalAutopsyStats vas = null;

        Cursor cursor = database.query(VerbalAutopsyStats.class, whereClause, clauseArgs, null, null, null);

        if (cursor.moveToFirst()){
            vas = Converter.cursorToVerbalAutopsyStats(cursor);
        }

        return vas;
    }

    public static List<VerbalAutopsyStats> getAllVerbalAutopsyStatsBy(Database database, String whereClause, String[] clauseArgs){
        List<VerbalAutopsyStats> list = new ArrayList<>();

        Cursor cursor = database.query(VerbalAutopsyStats.class, whereClause, clauseArgs, null, null, null);

        while (cursor.moveToNext()){
            VerbalAutopsyStats vas = Converter.cursorToVerbalAutopsyStats(cursor);
            list.add(vas);
        }

        return list;
    }

    public static ApplicationParam getApplicationParamBy(Database database, String whereClause, String[] clauseArgs){
        ApplicationParam param = null;

        Cursor cursor = database.query(ApplicationParam.class, whereClause, clauseArgs, null, null, null);

        if (cursor.moveToFirst()){
            param = Converter.cursorToApplicationParam(cursor);
        }

        return param;
    }

    public static List<ApplicationParam> getAllApplicationParamBy(Database database, String whereClause, String[] clauseArgs){
        List<ApplicationParam> list = new ArrayList<>();

        Cursor cursor = database.query(ApplicationParam.class, whereClause, clauseArgs, null, null, null);

        while (cursor.moveToNext()){
            ApplicationParam param = Converter.cursorToApplicationParam(cursor);
            list.add(param);
        }

        return list;
    }

    public static String getApplicationParamValue(String name, Context context){
        String value = null;

        Database database = new Database(context);
        database.open();

        ApplicationParam param = getApplicationParamBy(database, DatabaseHelper.ApplicationParam.COLUMN_NAME+"=?", new String[]{ name });

        if (param != null){
            value = param.getValue();
        }

        database.close();

        return value;
    }

}
