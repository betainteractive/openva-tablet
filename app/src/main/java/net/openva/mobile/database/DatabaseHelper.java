package net.openva.mobile.database;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

/*
 * This class are responsible for creating and altering database
 */
public class DatabaseHelper extends SQLiteOpenHelper {

		
	public DatabaseHelper(Context context) {
		super(context, Database.DATABASE_NAME, null, Database.DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
        try {

            db.execSQL(CREATE_TABLE_COLLECTED_DATA);
            db.execSQL(CREATE_TABLE_SYNC_REPORT);
		    db.execSQL(CREATE_TABLE_USER);
		    db.execSQL(CREATE_TABLE_INDIVIDUAL);
            db.execSQL(CREATE_TABLE_VA_CONTROL);
            db.execSQL(CREATE_TABLE_VA_STATS);
            db.execSQL(CREATE_APPLICATION_PARAM);

        }catch (Exception ex){
            ex.printStackTrace();
        }
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (newVersion > oldVersion) {
            /*
            Log.d("alter table", "runinng"); //OLD VERSION
            try {
                db.execSQL("ALTER TABLE " + CollectedData.TABLE_NAME + " ADD COLUMN " + CollectedData.COLUMN_SUPERVISED + " INTEGER NOT NULL DEFAULT 0"); //upgrade CollectedData
            }catch (Exception ex){
                Log.d("error on database alter", ""+ex.getMessage());
                ex.printStackTrace();
            }

            //NEW DB VERSION with BindMap
            try {
                db.execSQL("ALTER TABLE " + Form.TABLE_NAME + " ADD COLUMN " + Form.COLUMN_BIND_MAP + " TEXT"); //upgrade CollectedData
            }catch (Exception ex){
                Log.d("error on database alter", ""+ex.getMessage());
                ex.printStackTrace();
            }
            */
        }
	}

    public static final String[] ALL_TABLES = {User.TABLE_NAME, Individual.TABLE_NAME, SyncReport.TABLE_NAME };

    public static final class SyncReport implements BaseColumns {
        public static final String TABLE_NAME = "sync_report";

        public static final String COLUMN_REPORT_ID = "reportId";
        public static final String COLUMN_DESCRIPTION = "description";
        public static final String COLUMN_DATE = "date";
        public static final String COLUMN_STATUS = "status";

        public static final String[] ALL_COLUMNS = {COLUMN_REPORT_ID, COLUMN_DESCRIPTION, COLUMN_DATE, COLUMN_STATUS};
    }

    public static final class CollectedData implements BaseColumns {
        public static final String TABLE_NAME = "collected_data";

        public static final String COLUMN_FORM_ID = "formId";
        public static final String COLUMN_FORM_URI = "formUri";
        public static final String COLUMN_FORM_XML_PATH = "formXmlPath";
        public static final String COLUMN_RECORD_ID = "recordId";
        public static final String COLUMN_TABLE_NAME = "tableName";
        public static final String COLUMN_SUPERVISED = "supervised";

        public static final String[] ALL_COLUMNS = {_ID, COLUMN_FORM_ID, COLUMN_FORM_URI, COLUMN_FORM_XML_PATH, COLUMN_RECORD_ID, COLUMN_TABLE_NAME, COLUMN_SUPERVISED};
    }

	public static final class User implements BaseColumns {
		public static final String TABLE_NAME = "user";

        public static final String COLUMN_FIRSTNAME = "firstName";
        public static final String COLUMN_LASTNAME = "lastName";
        public static final String COLUMN_FULLNAME = "fullName";
		public static final String COLUMN_USERNAME = "username";
		public static final String COLUMN_PASSWORD = "password";
        public static final String COLUMN_ROLES = "modules";
        public static final String COLUMN_TOTAL_COLLECTED = "totalCollected";
        public static final String COLUMN_TOTAL_ATTEMPTS = "totalAttempts";

		public static final String[] ALL_COLUMNS = {COLUMN_FIRSTNAME, COLUMN_LASTNAME, COLUMN_FULLNAME, COLUMN_USERNAME, COLUMN_PASSWORD, COLUMN_ROLES, COLUMN_TOTAL_COLLECTED, COLUMN_TOTAL_ATTEMPTS};
	}

	public static final class Individual implements BaseColumns  {
		public static final String TABLE_NAME = "individual";

        public static final String COLUMN_EXT_ID = "extId";
        public static final String COLUMN_CODE = "code";
        public static final String COLUMN_NAME = "name";
        public static final String COLUMN_GENDER = "gender";
        public static final String COLUMN_DATE_OF_BIRTH = "dateOfBirth";
        public static final String COLUMN_AGE = "age";

        public static final String COLUMN_MOTHER_ID = "motherId";
        public static final String COLUMN_MOTHER_NAME = "motherName";
        public static final String COLUMN_FATHER_ID = "fatherId";
        public static final String COLUMN_FATHER_NAME = "fatherName";

        public static final String COLUMN_HOUSEHOLD_ID = "householdId";
        public static final String COLUMN_HOUSEHOLD_NO = "householdNo";

        public static final String COLUMN_GPS_AVAILABLE = "gpsAvailable";
        public static final String COLUMN_GPS_ACCURACY = "gpsAccuracy";
        public static final String COLUMN_GPS_ALTITUDE = "gpsAltitude";
        public static final String COLUMN_GPS_LATITUDE = "gpsLatitude";
        public static final String COLUMN_GPS_LONGITUDE = "gpsLongitude";

        public static final String COLUMN_COS_LATITUDE = "cosLatitude";
        public static final String COLUMN_SIN_LATITUDE = "sinLatitude";
        public static final String COLUMN_COS_LONGITUDE = "cosLongitude";
        public static final String COLUMN_SIN_LONGITUDE = "sinLongitude";


        public static final String[] ALL_COLUMNS = {_ID, COLUMN_EXT_ID, COLUMN_CODE, COLUMN_NAME, COLUMN_GENDER, COLUMN_DATE_OF_BIRTH, COLUMN_AGE, COLUMN_MOTHER_ID,
                COLUMN_MOTHER_NAME, COLUMN_FATHER_ID, COLUMN_FATHER_NAME, COLUMN_HOUSEHOLD_ID, COLUMN_HOUSEHOLD_NO,
                COLUMN_GPS_AVAILABLE, COLUMN_GPS_ACCURACY, COLUMN_GPS_ALTITUDE, COLUMN_GPS_LATITUDE, COLUMN_GPS_LONGITUDE,
                COLUMN_COS_LATITUDE, COLUMN_SIN_LATITUDE, COLUMN_COS_LONGITUDE, COLUMN_SIN_LONGITUDE};
	}

    public static final class VerbalAutopsyControl implements BaseColumns {
        public static final String TABLE_NAME = "va_control";

        public static final String COLUMN_EXT_ID = "extId";
        public static final String COLUMN_CODE = "code";
        public static final String COLUMN_NAME = "name";
        public static final String COLUMN_GENDER = "gender";
        public static final String COLUMN_DATE_OF_BIRTH = "dateOfBirth";
        public static final String COLUMN_DATE_OF_DEATH = "dateOfDeath";
        public static final String COLUMN_AGE_AT_DEATH = "ageAtDeath";

        public static final String COLUMN_MOTHER_ID = "motherId";
        public static final String COLUMN_MOTHER_NAME = "motherName";
        public static final String COLUMN_FATHER_ID = "fatherId";
        public static final String COLUMN_FATHER_NAME = "fatherName";

        public static final String COLUMN_HOUSEHOLD_ID = "householdId";
        public static final String COLUMN_HOUSEHOLD_NO = "householdNo";

        public static final String COLUMN_GPS_AVAILABLE = "gpsAvailable";
        public static final String COLUMN_GPS_ACCURACY = "gpsAccuracy";
        public static final String COLUMN_GPS_ALTITUDE = "gpsAltitude";
        public static final String COLUMN_GPS_LATITUDE = "gpsLatitude";
        public static final String COLUMN_GPS_LONGITUDE = "gpsLongitude";

        public static final String COLUMN_VA_TYPE = "vaType";
        public static final String COLUMN_VA_COLLECTED = "vaCollected";
        public static final String COLUMN_VA_PROCESSED_WITH = "vaProcessedWith";
        public static final String COLUMN_VA_PROCESSED = "vaProcessed";
        public static final String COLUMN_VA_RESULT = "vaResult";
        public static final String COLUMN_VA_ATTEMPTS = "vaAttempts";
        public static final String COLUMN_LAST_ATTEMPT_DATE = "lastAttemptDate";
        public static final String COLUMN_LAST_ATTEMPT_REASON = "lastAttemptReason";

        public static final String COLUMN_COS_LATITUDE = "cosLatitude";
        public static final String COLUMN_SIN_LATITUDE = "sinLatitude";
        public static final String COLUMN_COS_LONGITUDE = "cosLongitude";
        public static final String COLUMN_SIN_LONGITUDE = "sinLongitude";

        public static final String[] ALL_COLUMNS = {_ID, COLUMN_EXT_ID, COLUMN_CODE, COLUMN_NAME, COLUMN_GENDER, COLUMN_DATE_OF_BIRTH, COLUMN_DATE_OF_DEATH, COLUMN_AGE_AT_DEATH,
                COLUMN_MOTHER_ID, COLUMN_MOTHER_NAME, COLUMN_FATHER_ID, COLUMN_FATHER_NAME, COLUMN_HOUSEHOLD_ID, COLUMN_HOUSEHOLD_NO,
                COLUMN_GPS_AVAILABLE, COLUMN_GPS_ACCURACY, COLUMN_GPS_ALTITUDE, COLUMN_GPS_LATITUDE, COLUMN_GPS_LONGITUDE, COLUMN_VA_TYPE,
                COLUMN_VA_COLLECTED, COLUMN_VA_PROCESSED_WITH, COLUMN_VA_PROCESSED, COLUMN_VA_RESULT, COLUMN_VA_ATTEMPTS, COLUMN_LAST_ATTEMPT_DATE, COLUMN_LAST_ATTEMPT_REASON,
                COLUMN_COS_LATITUDE, COLUMN_SIN_LATITUDE, COLUMN_COS_LONGITUDE, COLUMN_SIN_LONGITUDE
        };
    }

    public static final class VerbalAutopsyStats implements BaseColumns {
        public static final String TABLE_NAME = "va_stats";


        public static final String COLUMN_CREATION_DATE = "creationDate";
        public static final String COLUMN_TOTAL_DEATHS = "totalDeaths";
        public static final String COLUMN_TOTAL_TO_COLLECT = "totalToCollect";
        public static final String COLUMN_TOTAL_COLLECTED = "totalCollected";
        public static final String COLUMN_TOTAL_ATTEMPTS = "totalAttempts";
        public static final String COLUMN_TOTAL_ATTEMPT_NI = "totalAttemptNi";
        public static final String COLUMN_TOTAL_ATTEMPT_UH = "totalAttemptUh";
        public static final String COLUMN_TOTAL_ATTEMPT_WDR = "totalAttemptWdr";
        public static final String COLUMN_TOTAL_ATTEMPT_OT = "totalAttemptOt";
        public static final String COLUMN_TOTAL_ATTEMPT_NC = "totalAttemptNc";

        public static final String[] ALL_COLUMNS = {
                _ID, COLUMN_CREATION_DATE, COLUMN_TOTAL_DEATHS, COLUMN_TOTAL_TO_COLLECT, COLUMN_TOTAL_COLLECTED,
                COLUMN_TOTAL_ATTEMPTS, COLUMN_TOTAL_ATTEMPT_NI, COLUMN_TOTAL_ATTEMPT_UH, COLUMN_TOTAL_ATTEMPT_WDR, COLUMN_TOTAL_ATTEMPT_OT, COLUMN_TOTAL_ATTEMPT_NC
        };
    }

    public static final class ApplicationParam implements BaseColumns {
        public static final String TABLE_NAME = "application_param";

        public static final String COLUMN_NAME = "name";
        public static final String COLUMN_TYPE = "type";
        public static final String COLUMN_VALUE = "value";

        public static final String[] ALL_COLUMNS = {
                _ID, COLUMN_NAME, COLUMN_TYPE, COLUMN_VALUE
        };
    }

    private static final String CREATE_TABLE_SYNC_REPORT = " "
            + "CREATE TABLE " + SyncReport.TABLE_NAME + "("
            + SyncReport._ID  + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + SyncReport.COLUMN_REPORT_ID + " INTEGER,"
            + SyncReport.COLUMN_DATE + " TEXT,"
            + SyncReport.COLUMN_STATUS + " INTEGER,"
            + SyncReport.COLUMN_DESCRIPTION + " TEXT);"

            + " CREATE UNIQUE INDEX IDX_REPORT_ID ON " + SyncReport.TABLE_NAME
            + "(" +  SyncReport.COLUMN_REPORT_ID  + ");"
            ;

    private static final String CREATE_TABLE_COLLECTED_DATA = " "
            + "CREATE TABLE " + CollectedData.TABLE_NAME + "("
            + CollectedData._ID  + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + CollectedData.COLUMN_FORM_ID + " TEXT,"
            + CollectedData.COLUMN_FORM_URI + " TEXT,"
            + CollectedData.COLUMN_FORM_XML_PATH + " TEXT,"
            + CollectedData.COLUMN_RECORD_ID + " INTEGER,"
            + CollectedData.COLUMN_TABLE_NAME + " TEXT,"
            + CollectedData.COLUMN_SUPERVISED + " INTEGER NOT NULL DEFAULT 0);"

            + " CREATE UNIQUE INDEX IDX_FORM_URI ON " + CollectedData.TABLE_NAME
            + "(" +  CollectedData.COLUMN_FORM_URI + ");"
            ;

    private static final String CREATE_TABLE_USER = " "
            + "CREATE TABLE " + User.TABLE_NAME + "("
            + User._ID  + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + User.COLUMN_FIRSTNAME + " TEXT,"
            + User.COLUMN_LASTNAME + " TEXT,"
            + User.COLUMN_FULLNAME + " TEXT,"
            + User.COLUMN_USERNAME + " TEXT,"
            + User.COLUMN_PASSWORD + " TEXT,"
            + User.COLUMN_ROLES + " TEXT,"
            + User.COLUMN_TOTAL_COLLECTED + " INTEGER,"
            + User.COLUMN_TOTAL_ATTEMPTS + " INTEGER);"

            + " CREATE UNIQUE INDEX IDX_USER_USERNAME ON " + User.TABLE_NAME
            + "(" +  User.COLUMN_USERNAME + ");"
            ;

    private static final String CREATE_TABLE_INDIVIDUAL = " "
            + "CREATE TABLE " + Individual.TABLE_NAME + "("
            + Individual._ID  + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + Individual.COLUMN_EXT_ID + " TEXT,"
            + Individual.COLUMN_CODE + " TEXT,"
            + Individual.COLUMN_NAME + " TEXT,"
            + Individual.COLUMN_GENDER + " TEXT,"
            + Individual.COLUMN_DATE_OF_BIRTH + " TEXT,"
            + Individual.COLUMN_AGE + " INTEGER,"

            + Individual.COLUMN_MOTHER_ID + " TEXT,"
            + Individual.COLUMN_MOTHER_NAME + " TEXT,"
            + Individual.COLUMN_FATHER_ID + " TEXT,"
            + Individual.COLUMN_FATHER_NAME + " TEXT,"

            + Individual.COLUMN_HOUSEHOLD_ID + " TEXT,"
            + Individual.COLUMN_HOUSEHOLD_NO + " TEXT,"

            + Individual.COLUMN_GPS_AVAILABLE + " INTEGER,"

            + Individual.COLUMN_GPS_ACCURACY + " REAL,"
            + Individual.COLUMN_GPS_ALTITUDE + " REAL,"
            + Individual.COLUMN_GPS_LATITUDE + " REAL,"
            + Individual.COLUMN_GPS_LONGITUDE + " REAL,"

            + Individual.COLUMN_COS_LATITUDE + " REAL,"
            + Individual.COLUMN_SIN_LATITUDE + " REAL,"
            + Individual.COLUMN_COS_LONGITUDE + " REAL,"
            + Individual.COLUMN_SIN_LONGITUDE + " REAL);"

            + " CREATE UNIQUE INDEX IDX_INDV_EXT_ID ON " + Individual.TABLE_NAME
            + "(" +  Individual.COLUMN_EXT_ID + ");"

            + " CREATE UNIQUE INDEX IDX_INDV_CODE ON " + Individual.TABLE_NAME
            + "(" +  Individual.COLUMN_CODE + ");"
            ;

    private static final String CREATE_TABLE_VA_CONTROL = " "
            + "CREATE TABLE " + VerbalAutopsyControl.TABLE_NAME + "("
            + VerbalAutopsyControl._ID  + " INTEGER PRIMARY KEY AUTOINCREMENT, "

            + VerbalAutopsyControl.COLUMN_EXT_ID + " TEXT,"
            + VerbalAutopsyControl.COLUMN_CODE + " TEXT,"
            + VerbalAutopsyControl.COLUMN_NAME + " TEXT,"
            + VerbalAutopsyControl.COLUMN_GENDER + " TEXT,"
            + VerbalAutopsyControl.COLUMN_DATE_OF_BIRTH + " TEXT,"
            + VerbalAutopsyControl.COLUMN_DATE_OF_DEATH + " TEXT,"
            + VerbalAutopsyControl.COLUMN_AGE_AT_DEATH + " INTEGER,"

            + VerbalAutopsyControl.COLUMN_MOTHER_ID + " TEXT,"
            + VerbalAutopsyControl.COLUMN_MOTHER_NAME + " TEXT,"
            + VerbalAutopsyControl.COLUMN_FATHER_ID + " TEXT,"
            + VerbalAutopsyControl.COLUMN_FATHER_NAME + " TEXT,"

            + VerbalAutopsyControl.COLUMN_HOUSEHOLD_ID + " TEXT,"
            + VerbalAutopsyControl.COLUMN_HOUSEHOLD_NO + " TEXT,"

            + VerbalAutopsyControl.COLUMN_GPS_AVAILABLE + " INTEGER,"
            + VerbalAutopsyControl.COLUMN_GPS_ACCURACY + " REAL,"
            + VerbalAutopsyControl.COLUMN_GPS_ALTITUDE + " REAL,"
            + VerbalAutopsyControl.COLUMN_GPS_LATITUDE + " REAL,"
            + VerbalAutopsyControl.COLUMN_GPS_LONGITUDE + " REAL,"

            + VerbalAutopsyControl.COLUMN_VA_TYPE + " TEXT,"
            + VerbalAutopsyControl.COLUMN_VA_COLLECTED + " TEXT,"
            + VerbalAutopsyControl.COLUMN_VA_PROCESSED_WITH + " TEXT,"
            + VerbalAutopsyControl.COLUMN_VA_PROCESSED + " TEXT,"
            + VerbalAutopsyControl.COLUMN_VA_RESULT + " TEXT,"
            + VerbalAutopsyControl.COLUMN_VA_ATTEMPTS + " TEXT,"

            + VerbalAutopsyControl.COLUMN_LAST_ATTEMPT_DATE + " TEXT,"
            + VerbalAutopsyControl.COLUMN_LAST_ATTEMPT_REASON + " TEXT,"

            + VerbalAutopsyControl.COLUMN_COS_LATITUDE + " REAL,"
            + VerbalAutopsyControl.COLUMN_SIN_LATITUDE + " REAL,"
            + VerbalAutopsyControl.COLUMN_COS_LONGITUDE + " REAL,"
            + VerbalAutopsyControl.COLUMN_SIN_LONGITUDE + " REAL);"

            + " CREATE UNIQUE INDEX IDX_VAC_EXT_ID ON " + VerbalAutopsyControl.TABLE_NAME
            + "(" +  VerbalAutopsyControl.COLUMN_EXT_ID + ");"

            + " CREATE UNIQUE INDEX IDX_VAC_CODE ON " + VerbalAutopsyControl.TABLE_NAME
            + "(" +  VerbalAutopsyControl.COLUMN_CODE + ");"
            ;

    private static final String CREATE_TABLE_VA_STATS = " "
            + "CREATE TABLE " + VerbalAutopsyStats.TABLE_NAME + "("
            + VerbalAutopsyStats._ID  + " INTEGER PRIMARY KEY AUTOINCREMENT, "

            + VerbalAutopsyStats.COLUMN_CREATION_DATE + " TEXT,"
            + VerbalAutopsyStats.COLUMN_TOTAL_DEATHS + " INTEGER,"
            + VerbalAutopsyStats.COLUMN_TOTAL_TO_COLLECT + " INTEGER,"
            + VerbalAutopsyStats.COLUMN_TOTAL_COLLECTED + " INTEGER,"
            + VerbalAutopsyStats.COLUMN_TOTAL_ATTEMPTS + " INTEGER,"
            + VerbalAutopsyStats.COLUMN_TOTAL_ATTEMPT_NI + " INTEGER,"
            + VerbalAutopsyStats.COLUMN_TOTAL_ATTEMPT_UH + " INTEGER,"
            + VerbalAutopsyStats.COLUMN_TOTAL_ATTEMPT_WDR + " INTEGER,"
            + VerbalAutopsyStats.COLUMN_TOTAL_ATTEMPT_OT + " INTEGER,"
            + VerbalAutopsyStats.COLUMN_TOTAL_ATTEMPT_NC + " INTEGER);"
            ;

    private static final String CREATE_APPLICATION_PARAM = " "
            + "CREATE TABLE " + ApplicationParam.TABLE_NAME + "("
            + ApplicationParam._ID  + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + ApplicationParam.COLUMN_NAME + " TEXT,"
            + ApplicationParam.COLUMN_TYPE + " TEXT,"
            + ApplicationParam.COLUMN_VALUE + " TEXT);"
            ;

}
