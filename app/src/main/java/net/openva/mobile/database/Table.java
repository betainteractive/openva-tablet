package net.openva.mobile.database;

import android.content.ContentValues;

public interface Table {

	int getId();

	String getTableName();
	
	ContentValues getContentValues();
	
	String[] getColumnNames();
}
