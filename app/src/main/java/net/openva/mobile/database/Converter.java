package net.openva.mobile.database;

import android.database.Cursor;

import net.openva.mobile.model.ApplicationParam;
import net.openva.mobile.model.CollectedData;
import net.openva.mobile.model.Individual;
import net.openva.mobile.model.SyncReport;
import net.openva.mobile.model.User;
import net.openva.mobile.model.VerbalAutopsyControl;
import net.openva.mobile.model.VerbalAutopsyStats;


public class Converter {

	public static SyncReport cursorToSyncReport(Cursor cursor){
		SyncReport syncReport = new SyncReport();

		syncReport.setReportId(Integer.parseInt(cursor.getString(cursor.getColumnIndex(DatabaseHelper.SyncReport.COLUMN_REPORT_ID))));
		syncReport.setDescription(cursor.getString(cursor.getColumnIndex(DatabaseHelper.SyncReport.COLUMN_DESCRIPTION)));
		syncReport.setDate(cursor.getString(cursor.getColumnIndex(DatabaseHelper.SyncReport.COLUMN_DATE)));
		syncReport.setStatus(cursor.getInt(cursor.getColumnIndex(DatabaseHelper.SyncReport.COLUMN_STATUS)));

		return syncReport;
	}

    public static CollectedData cursorToCollectedData(Cursor cursor){
        CollectedData cd = new CollectedData();

        cd.setId(cursor.getInt(cursor.getColumnIndex(DatabaseHelper.CollectedData._ID)));
        cd.setFormId(cursor.getString(cursor.getColumnIndex(DatabaseHelper.CollectedData.COLUMN_FORM_ID)));
        cd.setFormUri(cursor.getString(cursor.getColumnIndex(DatabaseHelper.CollectedData.COLUMN_FORM_URI)));
        cd.setFormXmlPath(cursor.getString(cursor.getColumnIndex(DatabaseHelper.CollectedData.COLUMN_FORM_XML_PATH)));
        cd.setRecordId(cursor.getInt(cursor.getColumnIndex(DatabaseHelper.CollectedData.COLUMN_RECORD_ID)));
        cd.setTableName(cursor.getString(cursor.getColumnIndex(DatabaseHelper.CollectedData.COLUMN_TABLE_NAME)));
        cd.setSupervised(cursor.getInt(cursor.getColumnIndex(DatabaseHelper.CollectedData.COLUMN_SUPERVISED))==1);

        return cd;
    }

	public static User cursorToUser(Cursor cursor){
		User user = new User();
				
		user.setFirstName(cursor.getString(cursor.getColumnIndex(DatabaseHelper.User.COLUMN_FIRSTNAME)));
		user.setLastName(cursor.getString(cursor.getColumnIndex(DatabaseHelper.User.COLUMN_LASTNAME)));
		user.setFullName(cursor.getString(cursor.getColumnIndex(DatabaseHelper.User.COLUMN_FULLNAME)));
		user.setUsername(cursor.getString(cursor.getColumnIndex(DatabaseHelper.User.COLUMN_USERNAME)));
		user.setPassword(cursor.getString(cursor.getColumnIndex(DatabaseHelper.User.COLUMN_PASSWORD)));
		user.setRoles(cursor.getString(cursor.getColumnIndex(DatabaseHelper.User.COLUMN_ROLES)));
		user.setTotalCollected(cursor.getInt(cursor.getColumnIndex(DatabaseHelper.User.COLUMN_TOTAL_COLLECTED)));
        user.setTotalAttempts(cursor.getInt(cursor.getColumnIndex(DatabaseHelper.User.COLUMN_TOTAL_ATTEMPTS)));
		
		return user;
	}

	public static Individual cursorToIndividual(Cursor cursor){
		Individual mb = new Individual();

		mb.setId(cursor.getInt(cursor.getColumnIndex(DatabaseHelper.Individual._ID)));

		mb.setExtId(cursor.getString(cursor.getColumnIndex(DatabaseHelper.Individual.COLUMN_EXT_ID)));
		mb.setCode(cursor.getString(cursor.getColumnIndex(DatabaseHelper.Individual.COLUMN_CODE)));
		mb.setName(cursor.getString(cursor.getColumnIndex(DatabaseHelper.Individual.COLUMN_NAME)));
		mb.setGender(cursor.getString(cursor.getColumnIndex(DatabaseHelper.Individual.COLUMN_GENDER)));
		mb.setDateOfBirth(cursor.getString(cursor.getColumnIndex(DatabaseHelper.Individual.COLUMN_DATE_OF_BIRTH)));
		mb.setAge(cursor.getInt(cursor.getColumnIndex(DatabaseHelper.Individual.COLUMN_AGE)));


		mb.setMotherId(cursor.getString(cursor.getColumnIndex(DatabaseHelper.Individual.COLUMN_MOTHER_ID)));
		mb.setMotherName(cursor.getString(cursor.getColumnIndex(DatabaseHelper.Individual.COLUMN_MOTHER_NAME)));

        mb.setFatherId(cursor.getString(cursor.getColumnIndex(DatabaseHelper.Individual.COLUMN_FATHER_ID)));
		mb.setFatherName(cursor.getString(cursor.getColumnIndex(DatabaseHelper.Individual.COLUMN_FATHER_NAME)));


        mb.setHouseholdId(cursor.getString(cursor.getColumnIndex(DatabaseHelper.Individual.COLUMN_HOUSEHOLD_ID)));
		mb.setHouseholdNo(cursor.getString(cursor.getColumnIndex(DatabaseHelper.Individual.COLUMN_HOUSEHOLD_NO)));

        mb.setGpsAvailable(cursor.getInt(cursor.getColumnIndex(DatabaseHelper.Individual.COLUMN_GPS_AVAILABLE))==1);
		mb.setGpsAccuracy(cursor.getDouble(cursor.getColumnIndex(DatabaseHelper.Individual.COLUMN_GPS_ACCURACY)));
		mb.setGpsAltitude(cursor.getDouble(cursor.getColumnIndex(DatabaseHelper.Individual.COLUMN_GPS_ALTITUDE)));
		mb.setGpsLatitude(cursor.getDouble(cursor.getColumnIndex(DatabaseHelper.Individual.COLUMN_GPS_LATITUDE)));
		mb.setGpsLongitude(cursor.getDouble(cursor.getColumnIndex(DatabaseHelper.Individual.COLUMN_GPS_LONGITUDE)));

        mb.setCosLatitude(cursor.getDouble(cursor.getColumnIndex(DatabaseHelper.Individual.COLUMN_COS_LATITUDE)));
		mb.setSinLatitude(cursor.getDouble(cursor.getColumnIndex(DatabaseHelper.Individual.COLUMN_SIN_LATITUDE)));
		mb.setCosLongitude(cursor.getDouble(cursor.getColumnIndex(DatabaseHelper.Individual.COLUMN_COS_LONGITUDE)));
		mb.setSinLongitude(cursor.getDouble(cursor.getColumnIndex(DatabaseHelper.Individual.COLUMN_SIN_LONGITUDE)));

		return mb;
	}

    public static VerbalAutopsyControl cursorToVerbalAutopsyControl(Cursor cursor) {
        VerbalAutopsyControl vac = new VerbalAutopsyControl();

		vac.setId(cursor.getInt(cursor.getColumnIndex(DatabaseHelper.VerbalAutopsyControl._ID)));

		vac.setExtId(cursor.getString(cursor.getColumnIndex(DatabaseHelper.VerbalAutopsyControl.COLUMN_EXT_ID)));
		vac.setCode(cursor.getString(cursor.getColumnIndex(DatabaseHelper.VerbalAutopsyControl.COLUMN_CODE)));
		vac.setName(cursor.getString(cursor.getColumnIndex(DatabaseHelper.VerbalAutopsyControl.COLUMN_NAME)));
		vac.setGender(cursor.getString(cursor.getColumnIndex(DatabaseHelper.VerbalAutopsyControl.COLUMN_GENDER)));
		vac.setDateOfBirth(cursor.getString(cursor.getColumnIndex(DatabaseHelper.VerbalAutopsyControl.COLUMN_DATE_OF_BIRTH)));
		vac.setDateOfDeath(cursor.getString(cursor.getColumnIndex(DatabaseHelper.VerbalAutopsyControl.COLUMN_DATE_OF_DEATH)));
		vac.setAgeAtDeath(cursor.getInt(cursor.getColumnIndex(DatabaseHelper.VerbalAutopsyControl.COLUMN_AGE_AT_DEATH)));

		vac.setMotherId(cursor.getString(cursor.getColumnIndex(DatabaseHelper.VerbalAutopsyControl.COLUMN_MOTHER_ID)));
		vac.setMotherName(cursor.getString(cursor.getColumnIndex(DatabaseHelper.VerbalAutopsyControl.COLUMN_MOTHER_NAME)));
		vac.setFatherId(cursor.getString(cursor.getColumnIndex(DatabaseHelper.VerbalAutopsyControl.COLUMN_FATHER_ID)));
		vac.setFatherName(cursor.getString(cursor.getColumnIndex(DatabaseHelper.VerbalAutopsyControl.COLUMN_FATHER_NAME)));

		vac.setHouseholdId(cursor.getString(cursor.getColumnIndex(DatabaseHelper.VerbalAutopsyControl.COLUMN_HOUSEHOLD_ID)));
		vac.setHouseholdNo(cursor.getString(cursor.getColumnIndex(DatabaseHelper.VerbalAutopsyControl.COLUMN_HOUSEHOLD_NO)));

		vac.setGpsAvailable(cursor.getInt(cursor.getColumnIndex(DatabaseHelper.VerbalAutopsyControl.COLUMN_GPS_AVAILABLE))==1);
		vac.setGpsAccuracy(cursor.getDouble(cursor.getColumnIndex(DatabaseHelper.VerbalAutopsyControl.COLUMN_GPS_ACCURACY)));
		vac.setGpsAltitude(cursor.getDouble(cursor.getColumnIndex(DatabaseHelper.VerbalAutopsyControl.COLUMN_GPS_ALTITUDE)));
		vac.setGpsLatitude(cursor.getDouble(cursor.getColumnIndex(DatabaseHelper.VerbalAutopsyControl.COLUMN_GPS_LATITUDE)));
		vac.setGpsLongitude(cursor.getDouble(cursor.getColumnIndex(DatabaseHelper.VerbalAutopsyControl.COLUMN_GPS_LONGITUDE)));

		vac.setVaType(cursor.getInt(cursor.getColumnIndex(DatabaseHelper.VerbalAutopsyControl.COLUMN_VA_TYPE)));
		vac.setVaCollected(cursor.getInt(cursor.getColumnIndex(DatabaseHelper.VerbalAutopsyControl.COLUMN_VA_COLLECTED)));
		vac.setVaProcessedWith(cursor.getString(cursor.getColumnIndex(DatabaseHelper.VerbalAutopsyControl.COLUMN_VA_PROCESSED_WITH)));
		vac.setVaProcessed(cursor.getInt(cursor.getColumnIndex(DatabaseHelper.VerbalAutopsyControl.COLUMN_VA_PROCESSED)));
		vac.setVaResult(cursor.getString(cursor.getColumnIndex(DatabaseHelper.VerbalAutopsyControl.COLUMN_VA_RESULT)));
		vac.setVaAttempts(cursor.getInt(cursor.getColumnIndex(DatabaseHelper.VerbalAutopsyControl.COLUMN_VA_ATTEMPTS)));

		vac.setLastAttemptDate(cursor.getString(cursor.getColumnIndex(DatabaseHelper.VerbalAutopsyControl.COLUMN_LAST_ATTEMPT_DATE)));
		vac.setLastAttemptReason(cursor.getString(cursor.getColumnIndex(DatabaseHelper.VerbalAutopsyControl.COLUMN_LAST_ATTEMPT_REASON)));

        return vac;
    }

    public static VerbalAutopsyStats cursorToVerbalAutopsyStats(Cursor cursor) {
        VerbalAutopsyStats vas = new VerbalAutopsyStats();
	
		vas.setId(cursor.getInt(cursor.getColumnIndex(DatabaseHelper.VerbalAutopsyStats._ID)));
		vas.setCreationDate(cursor.getString(cursor.getColumnIndex(DatabaseHelper.VerbalAutopsyStats.COLUMN_CREATION_DATE)));
		vas.setTotalDeaths(cursor.getInt(cursor.getColumnIndex(DatabaseHelper.VerbalAutopsyStats.COLUMN_TOTAL_DEATHS)));
		vas.setTotalToCollect(cursor.getInt(cursor.getColumnIndex(DatabaseHelper.VerbalAutopsyStats.COLUMN_TOTAL_TO_COLLECT)));
		vas.setTotalCollected(cursor.getInt(cursor.getColumnIndex(DatabaseHelper.VerbalAutopsyStats.COLUMN_TOTAL_COLLECTED)));
		vas.setTotalAttempts(cursor.getInt(cursor.getColumnIndex(DatabaseHelper.VerbalAutopsyStats.COLUMN_TOTAL_ATTEMPTS)));
		vas.setTotalAttemptNi(cursor.getInt(cursor.getColumnIndex(DatabaseHelper.VerbalAutopsyStats.COLUMN_TOTAL_ATTEMPT_NI)));
		vas.setTotalAttemptUh(cursor.getInt(cursor.getColumnIndex(DatabaseHelper.VerbalAutopsyStats.COLUMN_TOTAL_ATTEMPT_UH)));
		vas.setTotalAttemptWdr(cursor.getInt(cursor.getColumnIndex(DatabaseHelper.VerbalAutopsyStats.COLUMN_TOTAL_ATTEMPT_WDR)));
		vas.setTotalAttemptOt(cursor.getInt(cursor.getColumnIndex(DatabaseHelper.VerbalAutopsyStats.COLUMN_TOTAL_ATTEMPT_OT)));
		vas.setTotalAttemptNc(cursor.getInt(cursor.getColumnIndex(DatabaseHelper.VerbalAutopsyStats.COLUMN_TOTAL_ATTEMPT_NC)));

        return vas;
    }

	public static ApplicationParam cursorToApplicationParam(Cursor cursor) {
		ApplicationParam param = new ApplicationParam();

		param.setId(cursor.getInt(cursor.getColumnIndex(DatabaseHelper.ApplicationParam._ID)));
		param.setName(cursor.getString(cursor.getColumnIndex(DatabaseHelper.ApplicationParam.COLUMN_NAME)));
		param.setType(cursor.getString(cursor.getColumnIndex(DatabaseHelper.ApplicationParam.COLUMN_TYPE)));
		param.setValue(cursor.getString(cursor.getColumnIndex(DatabaseHelper.ApplicationParam.COLUMN_VALUE)));

		return param;
	}

	public static VerbalAutopsyControl createVerbalAutopsyControl(Individual individual){
		VerbalAutopsyControl vaControl = VerbalAutopsyControl.getEmptyVaControl();

		vaControl.setExtId(individual.getExtId());
		vaControl.setCode(individual.getCode());
		vaControl.setName(individual.getName());
		vaControl.setGender(individual.getGender());
		vaControl.setDateOfBirth(individual.getDateOfBirth());
		//vaControl.setDateOfDeath(individual.);
		vaControl.setAgeAtDeath(individual.getAge());

		vaControl.setMotherId(individual.getMotherId());
		vaControl.setMotherName(individual.getMotherName());
		vaControl.setFatherId(individual.getFatherId());
		vaControl.setFatherName(individual.getFatherName());

		vaControl.setHouseholdId(individual.getHouseholdId());
		vaControl.setHouseholdNo(individual.getHouseholdNo());

		vaControl.setGpsAvailable(individual.isGpsAvailable());
		vaControl.setGpsAccuracy(individual.getGpsAccuracy());
		vaControl.setGpsAltitude(individual.getGpsAltitude());
		vaControl.setGpsLatitude(individual.getGpsLatitude());
		vaControl.setGpsLongitude(individual.getGpsLongitude());

		//vaControl.setVaType(individual.);
		//vaControl.setVaCollected(individual.);
		//vaControl.setVaProcessedWith(individual.);
		//vaControl.setVaProcessed(individual.);
		//vaControl.setVaResult(individual.);

		//vaControl.setvaAttempts(individual.);
		//vaControl.setlastAttemptDate(individual.);
		//vaControl.setlastAttemptReason(individual.);

		vaControl.setCosLatitude(individual.getCosLatitude());
		vaControl.setSinLatitude(individual.getSinLatitude());
		vaControl.setCosLongitude(individual.getCosLongitude());
		vaControl.setSinLongitude(individual.getSinLongitude());

		return vaControl;
	}
}
