package net.openva.mobile.database;

import android.content.Context;
import android.database.Cursor;
import android.os.Environment;
import android.util.Log;

import net.openva.mobile.model.ApplicationParam;
import net.openva.mobile.model.SyncReport;

import java.io.File;

/**
 * Created by paul on 5/26/16.
 * Will be used to initialize any data on database tables
 */
public class Bootstrap {
    private static final String APP_PATH = "net.openva.mobile";
    private Database database;

    public Bootstrap(Context context){
        this.database = new Database(context);
    }

    public void init(){
        database.open();

        Cursor cursorReports = database.query(SyncReport.class, null, null, null, null, null);

        //Initialize SyncReport
        if (cursorReports.getCount()==0) {
            SyncReport sr1 = new SyncReport(SyncReport.REPORT_USERS, null, SyncReport.STATUS_NOT_SYNCED, "Sync. Users");
            SyncReport sr2 = new SyncReport(SyncReport.REPORT_INDIVIDUALS, null, SyncReport.STATUS_NOT_SYNCED, "Sync. Individuals");
            SyncReport sr3 = new SyncReport(SyncReport.REPORT_VA_CONTROLS, null, SyncReport.STATUS_NOT_SYNCED, "Sync. VA Controls");
            SyncReport sr4 = new SyncReport(SyncReport.REPORT_VA_STATS, null, SyncReport.STATUS_NOT_SYNCED, "Sync. Stats");

            database.insert(sr1);
            database.insert(sr2);
            database.insert(sr3);
            database.insert(sr4);
        }

        Cursor cursorParams = database.query(ApplicationParam.class, null, null, null, null, null);

        if (cursorParams.getCount()==0){
            ApplicationParam param1 = new ApplicationParam(ApplicationParam.OPENVA_URL, "string", "http://sap.manhica.net:4780/openva-server"); // OpenVA Server URL
            ApplicationParam param2 = new ApplicationParam(ApplicationParam.ODK_FORM_ID, "string", "who_va_2016");                               // WHO ODK FORM_ID
            ApplicationParam param3 = new ApplicationParam(ApplicationParam.REDCAP_URL, "string", "https://sap.manhica.net:4703/redcap");             // REDCap Server URL

            database.insert(param1);
            database.insert(param2);
            database.insert(param3);
        }

        database.close();

        initializePaths();
    }

    public void initializePaths(){
        File root = Environment.getExternalStorageDirectory();
        String destinationPath = root.getAbsolutePath() + File.separator + "Android" + File.separator + "data"
                + File.separator + APP_PATH + File.separator + "files"+ File.separator;

        File baseDir = new File(destinationPath);

        if (!baseDir.exists()) {
            boolean created = baseDir.mkdirs();
            if (!created) {
                Log.d("app-dirs", "not created");
            }else{
                Log.d("app-dirs", "created");
            }
        }
    }

    public static String getAppPath(){
        File root = Environment.getExternalStorageDirectory();
        String destinationPath = root.getAbsolutePath() + File.separator + "Android" + File.separator + "data"
                + File.separator + APP_PATH + File.separator + "files"+ File.separator;

        return destinationPath;
    }

    public void dropTables(){
        database.open();
        database.dropAllTables();
        database.close();
    }
}
