package net.openva.mobile.main;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import net.openva.mobile.R;
import net.openva.mobile.database.Database;
import net.openva.mobile.database.DatabaseHelper;
import net.openva.mobile.database.Queries;
import net.openva.mobile.database.Table;
import net.openva.mobile.io.xml.FormXmlReader;
import net.openva.mobile.model.ApplicationParam;
import net.openva.mobile.model.CollectedData;
import net.openva.mobile.model.Individual;
import net.openva.mobile.model.User;
import net.openva.mobile.model.VerbalAutopsyControl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import mz.betainteractive.odk.FormUtilities;
import mz.betainteractive.odk.listener.OdkFormResultListener;
import mz.betainteractive.odk.model.FilledForm;
import mz.betainteractive.utilities.StringUtil;

public class IndividualDetailsActivity extends Activity implements OdkFormResultListener {

    private String FORM_ID = "who_va_2016";

    private TextView idDetailsName;
    private TextView idDetailsCode;
    private TextView idDetailsGender;
    private TextView idDetailsAge;
    private TextView idDetailsDob;
    private TextView idDetailsHouseNo;
    private TextView idDetailsDthDate;
    private TextView idDetailsFather;
    private TextView idDetailsMother;
    private Button btIndDetailsCollectData;
    private Button btIndDetailsBack;
    private ImageView iconView;

    private User loggedUser;

    private VerbalAutopsyControl deadIndividual;
    private CollectedData collectedData;
    private boolean isPreRegistered = true;

    private FormUtilities formUtilities;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.individual_details);

        FORM_ID = Queries.getApplicationParamValue(ApplicationParam.ODK_FORM_ID, this);

        this.loggedUser = (User) getIntent().getExtras().get("user");

        Object obj = getIntent().getExtras().get("individual");
        Object obj2 = getIntent().getExtras().get("isPreRegistered");

        if (obj instanceof VerbalAutopsyControl){
            this.deadIndividual = (VerbalAutopsyControl) obj;
            this.collectedData = getCollectedData(this.deadIndividual);
        }

        if (obj2 != null){
            this.isPreRegistered = (boolean) obj2;
        }

        formUtilities = new FormUtilities(this);

        initialize();
    }

    public void setVerbalAutopsyControl(VerbalAutopsyControl vaControl){
        this.deadIndividual = vaControl;
    }

    private void initialize() {
        idDetailsName = (TextView) findViewById(R.id.idDetailsName);
        idDetailsCode = (TextView) findViewById(R.id.idDetailsCode);
        idDetailsGender = (TextView) findViewById(R.id.idDetailsGender);
        idDetailsAge = (TextView) findViewById(R.id.idDetailsAge);
        idDetailsDob = (TextView) findViewById(R.id.idDetailsDob);
        idDetailsHouseNo = (TextView) findViewById(R.id.idDetailsHouseNo);
        idDetailsDthDate = (TextView) findViewById(R.id.idDetailsDthDate);
        idDetailsFather = (TextView) findViewById(R.id.idDetailsFather);
        idDetailsMother = (TextView) findViewById(R.id.idDetailsMother);

        btIndDetailsCollectData = (Button) findViewById(R.id.btIndDetailsCollectData);
        btIndDetailsBack = (Button) findViewById(R.id.btIndDetailsBack);
        iconView = (ImageView) findViewById(R.id.iconView);

        btIndDetailsBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IndividualDetailsActivity.this.onBackPressed();
            }
        });

        btIndDetailsCollectData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onCollectDataClicked();
            }
        });

        setIndividualData();
    }

    private void setIndividualData(){
        if (deadIndividual != null){
            idDetailsName.setText(deadIndividual.getName());
            idDetailsCode.setText(deadIndividual.getCode());
            idDetailsGender.setText(deadIndividual.getGender());
            idDetailsAge.setText(deadIndividual.getAgeAtDeath()+"");
            idDetailsDob.setText(deadIndividual.getDateOfBirth());
            idDetailsHouseNo.setText(deadIndividual.getHouseholdNo());
            idDetailsDthDate.setText(getDthDate(deadIndividual.getDateOfDeath()));
            idDetailsFather.setText(getParentName(deadIndividual.getFatherName()));
            idDetailsMother.setText(getParentName(deadIndividual.getMotherName()));
        }
    }

    private String getDthDate(String date){
        if (date == null) return "";
        return date;
    }

    private String getParentName(String name){
        if (name.equals("Unknown")){
            return getString(R.string.individual_details_unknown_lbl);
        }else {
            return name;
        }
    }

    /*
     * Convert gender from character format (M, F) to WHO VA (male, female)
     */
    private String convertGender(String charFormat){
        if (charFormat.trim().equalsIgnoreCase("m")){
            return "male";
        }
        if (charFormat.trim().equalsIgnoreCase("f")){
            return "female";
        }

        return "";
    }

    private void onCollectDataClicked(){

        FilledForm filledForm = new FilledForm(FORM_ID);

        if (this.deadIndividual != null) {
            //load auto filled variables from Verbal Autopsy Control
            filledForm.put("isPreRegistered", this.isPreRegistered ? "yes" : "no");
            filledForm.put("fieldWorkerId", loggedUser.getUsername());
            filledForm.put("householdId", deadIndividual.getHouseholdNo());
            filledForm.put("individualId", deadIndividual.getCode());

            filledForm.put("Id10010", loggedUser.getFullName());

            String[] names = StringUtil.splitFullname(deadIndividual.getName());

            filledForm.put("Id10017", names[0]);
            filledForm.put("Id10018", names.length>1 ? names[1] : "");

            filledForm.put("Id10019", convertGender(deadIndividual.getGender()));
            filledForm.put("Id10020", "yes");
            filledForm.put("Id10021", deadIndividual.getDateOfBirth());
            filledForm.put("Id10061", deadIndividual.getFatherName());
            filledForm.put("Id10062", deadIndividual.getMotherName());

            if (deadIndividual.getDateOfDeath()!=null && !deadIndividual.getDateOfDeath().isEmpty()){
                filledForm.put("Id10023", deadIndividual.getDateOfDeath());
            }
        }

        openOdkForm(filledForm);
    }

    private CollectedData getCollectedData(Table table){
        Database db = new Database(this);
        db.open();

        String whereClause = DatabaseHelper.CollectedData.COLUMN_FORM_ID + "=? AND " + DatabaseHelper.CollectedData.COLUMN_RECORD_ID + "=? AND "+DatabaseHelper.CollectedData.COLUMN_TABLE_NAME + "=?";
        String[] whereArgs = new String[]{ FORM_ID,  ""+ table.getId(), table.getTableName() };

        CollectedData collectedData = Queries.getCollectedDataBy(db, whereClause, whereArgs);

        db.close();

        return collectedData;
    }

    private void openOdkForm(FilledForm filledForm) {
        if (collectedData == null){
            formUtilities.loadForm(filledForm);
        }else{

            if (formUtilities.formExists(collectedData.getFormUri())){
                formUtilities.loadForm(filledForm, collectedData.getFormUri());
            }else{
                //for some unknown reason the form was deleted

                buildOkDialog(getString(R.string.individual_details_savedform_not_found));

                //delete the collected data because real xml form was deleted
                onDeleteForm(Uri.parse(collectedData.getFormUri()));

                //open a new blank form to be collected
                formUtilities.loadForm(filledForm);
            }


        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        formUtilities.onActivityResult(requestCode, resultCode, data, this);
    }

    @Override
    public void onFormFinalized(Uri contentUri, File xmlFile) {
        Log.d("form finalized"," "+contentUri+", "+xmlFile);

        //save unsaved individual
        saveIndividualToDatabase(xmlFile);

        //save Collected data
        Database db = new Database(this);
        db.open();
        //update or insert

        //search existing record
        String whereClause = DatabaseHelper.CollectedData.COLUMN_RECORD_ID + "=? AND "+DatabaseHelper.CollectedData.COLUMN_FORM_URI + "=?";
        String[] whereArgs = new String[]{ ""+ deadIndividual.getId(), contentUri.toString() };

        CollectedData collectedData = Queries.getCollectedDataBy(db, whereClause, whereArgs);

        if (collectedData == null){ //insert
            collectedData = new CollectedData();
            collectedData.setFormId(FORM_ID);
            collectedData.setFormUri(contentUri.toString());
            collectedData.setFormXmlPath(xmlFile.toString());
            collectedData.setRecordId(deadIndividual.getId());
            collectedData.setTableName(deadIndividual.getTableName());

            db.insert(collectedData);
            Log.d("inserting", "new collected data");
        }else{ //update
            collectedData.setFormId(FORM_ID);
            collectedData.setFormUri(contentUri.toString());
            collectedData.setFormXmlPath(xmlFile.toString());
            collectedData.setRecordId(deadIndividual.getId());
            collectedData.setTableName(deadIndividual.getTableName());

            db.update(CollectedData.class, collectedData.getContentValues(), whereClause, whereArgs);
            Log.d("updating", "new collected data");
        }

        db.close();
    }

    @Override
    public void onFormUnFinalized(Uri contentUri, File xmlFile) {
        Log.d("form unfinalized"," "+contentUri);

        //save unsaved individual
        saveIndividualToDatabase(xmlFile);

        //save Collected data
        Database db = new Database(this);
        db.open();
        //update or insert

        //search existing record
        String whereClause = DatabaseHelper.CollectedData.COLUMN_RECORD_ID + "=? AND "+DatabaseHelper.CollectedData.COLUMN_FORM_URI + "=?";
        String[] whereArgs = new String[]{ ""+ deadIndividual.getId(), contentUri.toString() };

        CollectedData collectedData = Queries.getCollectedDataBy(db, whereClause, whereArgs);

        if (collectedData == null){ //insert
            collectedData = new CollectedData();
            collectedData.setFormId(FORM_ID);
            collectedData.setFormUri(contentUri.toString());
            collectedData.setFormXmlPath("");
            collectedData.setRecordId(deadIndividual.getId());
            collectedData.setTableName(deadIndividual.getTableName());

            db.insert(collectedData);
            //Log.d("inserting", "new collected data");
        }else{ //update
            collectedData.setFormId(FORM_ID);
            collectedData.setFormUri(contentUri.toString());
            collectedData.setFormXmlPath("");
            collectedData.setRecordId(deadIndividual.getId());
            collectedData.setTableName(deadIndividual.getTableName());

            db.update(CollectedData.class, collectedData.getContentValues(), whereClause, whereArgs);
            //Log.d("updating", "new collected data");
        }

        db.close();
    }

    @Override
    public void onDeleteForm(Uri contentUri) {
        Log.d("delete uri", "needs to be implemented");
        Database db = new Database(this);
        db.open();
        db.delete(CollectedData.class, DatabaseHelper.CollectedData.COLUMN_FORM_URI+"=?", new String[]{ contentUri.toString() } );
        db.close();
    }

    private void saveIndividualToDatabase(File xmlFile){

        if (deadIndividual == null) return;

        String dthDateXml = getDeathDateFromXML(xmlFile);
        boolean dodUpdated = false;

        //update death-date
        if (!StringUtil.isNullOrEmpty(dthDateXml) && StringUtil.isNullOrEmpty(deadIndividual.getDateOfDeath()) ){
            deadIndividual.setDateOfDeath(dthDateXml);
            dodUpdated = true;
        }

        //save deadIndividual - to database (insert or update)
        Database db = new Database(this);
        db.open();

        if (deadIndividual.getId()==0){
            long id = db.insert(deadIndividual);
            deadIndividual.setId((int)id);
        }else if (dodUpdated){
            db.update(VerbalAutopsyControl.class, deadIndividual.getContentValues(), DatabaseHelper.VerbalAutopsyControl._ID+"=?", new String[] { deadIndividual.getId()+"" });
        }

        db.close();

        //refresh individual data
        setIndividualData();
    }

    private String getDeathDateFromXML(File xmlFile){
        try {

            FormXmlReader xmlReader = new FormXmlReader();
            FileInputStream fis = new FileInputStream(xmlFile);
            String dthdate = xmlReader.readDeathDateFromWhoVA(fis, FORM_ID);
            return dthdate;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return "";
    }

    private void buildOkDialog(String message) {
        buildOkDialog(null, message);
    }

    private void buildOkDialog(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        title = (title == null || title.isEmpty()) ? getString(R.string.info_lbl) : title;

        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);
        builder.setPositiveButton("OK", null);
        builder.show();
    }
}
