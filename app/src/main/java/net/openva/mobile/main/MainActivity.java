package net.openva.mobile.main;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import net.openva.mobile.R;
import net.openva.mobile.fragment.CreateIndividualFragment;
import net.openva.mobile.model.User;
import net.openva.mobile.model.VerbalAutopsyControl;

public class MainActivity extends Activity implements CreateIndividualFragment.Listener {
    private Button btMainRegDeaths;
    private Button btMainUnRegDeaths;
    private Button btMainUnregIndivs;
    private Button btMainStats;
    private User loggedUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        initialize();
    }

    private void initialize() {

        this.loggedUser = (User) getIntent().getExtras().get("user");

        this.btMainRegDeaths = (Button) findViewById(R.id.btMainRegDeaths);
        this.btMainUnRegDeaths = (Button) findViewById(R.id.btMainUnRegDeaths);
        this.btMainUnregIndivs = (Button) findViewById(R.id.btMainUnregIndivs);
        this.btMainStats = (Button) findViewById(R.id.btMainStats);

        this.btMainRegDeaths.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openRegDeaths();
            }
        });

        this.btMainUnRegDeaths.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openUnregDeaths();
            }
        });

        this.btMainUnregIndivs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openUnregIndividuals();
            }
        });

        this.btMainStats.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openStats();
            }
        });

    }

    private void openRegDeaths(){
        Intent intent = new Intent(this, RegisteredDeathsActivity.class);
        intent.putExtra("user", loggedUser);
        startActivity(intent);
    }

    private void openUnregDeaths(){
        Intent intent = new Intent(this, UnregisteredDeathsActivity.class);
        intent.putExtra("user", loggedUser);
        startActivity(intent);
    }

    private void openUnregIndividuals(){
        new CreateIndividualFragment().show(getFragmentManager(), "dialog-new");
    }

    private void openStats(){
        Intent intent = new Intent(this, StatsActivity.class);
        intent.putExtra("user", loggedUser);
        startActivity(intent);
    }

    @Override
    public void onIndividualCreated(VerbalAutopsyControl deadIndividual) {
        Intent intent = new Intent(this, IndividualDetailsActivity.class);
        intent.putExtra("individual", deadIndividual);
        intent.putExtra("user", loggedUser);
        intent.putExtra("isPreRegistered", false); //not registered on HDSS
        startActivity(intent);
    }
}
