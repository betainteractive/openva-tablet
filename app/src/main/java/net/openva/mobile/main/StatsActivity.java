package net.openva.mobile.main;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import net.openva.mobile.R;
import net.openva.mobile.database.Database;
import net.openva.mobile.database.Queries;
import net.openva.mobile.model.User;
import net.openva.mobile.model.VerbalAutopsyStats;

import java.util.List;

public class StatsActivity extends Activity {

    private TextView txtStatsUserCollected;
    private TextView txtStatsUserAttempts;

    private TextView txtStatsGeneralDeaths;
    private TextView txtStatsGeneralCollected;
    private TextView txtStatsGeneralToCollect;

    private TextView txtStatsAttemptsToCol;
    private TextView txtStatsAttemptsNi;
    private TextView txtStatsAttemptsUh;
    private TextView txtStatsAttemptsWdr;
    private TextView txtStatsAttemptsOt;
    private TextView txtStatsAttemptsNc;
    private Button btStatsBack;

    private User loggedUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.stats);

        initialize();

        showStats();
    }

    private void initialize() {

        this.loggedUser = (User) getIntent().getExtras().get("user");

        this.txtStatsUserCollected = (TextView) findViewById(R.id.txtStatsUserCollected);
        this.txtStatsUserAttempts = (TextView) findViewById(R.id.txtStatsUserAttempts);

        this.txtStatsGeneralDeaths = (TextView) findViewById(R.id.txtStatsGeneralDeaths);
        this.txtStatsGeneralCollected = (TextView) findViewById(R.id.txtStatsGeneralCollected);
        this.txtStatsGeneralToCollect = (TextView) findViewById(R.id.txtStatsGeneralToCollect);

        this.txtStatsAttemptsToCol = (TextView) findViewById(R.id.txtStatsAttemptsToCol);
        this.txtStatsAttemptsNi = (TextView) findViewById(R.id.txtStatsAttemptsNi);
        this.txtStatsAttemptsUh = (TextView) findViewById(R.id.txtStatsAttemptsUh);
        this.txtStatsAttemptsWdr = (TextView) findViewById(R.id.txtStatsAttemptsWdr);
        this.txtStatsAttemptsOt = (TextView) findViewById(R.id.txtStatsAttemptsOt);
        this.txtStatsAttemptsNc = (TextView) findViewById(R.id.txtStatsAttemptsNc);

        this.btStatsBack = (Button) findViewById(R.id.btStatsBack);

        this.btStatsBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void showStats() {

        Database db = new Database(this);
        db.open();
        List<VerbalAutopsyStats> listStats = Queries.getAllVerbalAutopsyStatsBy(db, null, null);
        db.close();


        this.txtStatsUserCollected.setText(this.loggedUser.getTotalCollected()+"");
        this.txtStatsUserAttempts.setText(this.loggedUser.getTotalAttempts()+"");

        if (listStats.size() > 0){
            VerbalAutopsyStats stats = listStats.get(0);

            this.txtStatsGeneralDeaths.setText(""+stats.getTotalDeaths());
            this.txtStatsGeneralCollected.setText(""+stats.getTotalCollected());
            this.txtStatsGeneralToCollect.setText(""+stats.getTotalToCollect());

            this.txtStatsAttemptsToCol.setText(""+stats.getTotalAttempts());
            this.txtStatsAttemptsNi.setText(""+stats.getTotalAttemptNi());
            this.txtStatsAttemptsUh.setText(""+stats.getTotalAttemptUh());
            this.txtStatsAttemptsWdr.setText(""+stats.getTotalAttemptWdr());
            this.txtStatsAttemptsOt.setText(""+stats.getTotalAttemptOt());
            this.txtStatsAttemptsNc.setText(""+stats.getTotalAttemptNc());
        }
    }

}
