package net.openva.mobile.main;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

import net.openva.mobile.R;
import net.openva.mobile.adapter.IndividualArrayAdapter;
import net.openva.mobile.database.Converter;
import net.openva.mobile.fragment.IndividualFilterFragment;
import net.openva.mobile.fragment.IndividualListFragment;
import net.openva.mobile.listener.IndividualActionListener;
import net.openva.mobile.model.Individual;
import net.openva.mobile.model.User;
import net.openva.mobile.model.VerbalAutopsyControl;

public class UnregisteredDeathsActivity extends Activity implements IndividualFilterFragment.Listener, IndividualActionListener {

    private IndividualFilterFragment individualFilterFragment;
    private IndividualListFragment individualListFragment;

    private User loggedUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registered_deaths);

        this.loggedUser = (User) getIntent().getExtras().get("user");

        this.individualFilterFragment = (IndividualFilterFragment) (getFragmentManager().findFragmentById(R.id.individualFilterFragment));
        this.individualListFragment = (IndividualListFragment) (getFragmentManager().findFragmentById(R.id.individualListFragment));

        initialize();
    }

    private void initialize() {
        this.individualFilterFragment.setSearchFilter(IndividualFilterFragment.SearchFilter.RESIDENTS);
    }

    @Override
    public void onSearch(String name, String code, String householdNo, String gender, Integer minAge, Integer maxAge, boolean isDead, boolean isAlive) {
        this.individualListFragment.showProgress(true);

        IndividualSearchTask task = new IndividualSearchTask(name, code, householdNo, gender, minAge, maxAge);
        task.execute();
    }

    @Override
    public void onIndividualSelected(Individual individual) {

        VerbalAutopsyControl deadIndividual = Converter.createVerbalAutopsyControl(individual);

        Intent intent = new Intent(this, IndividualDetailsActivity.class);
        intent.putExtra("individual", deadIndividual);
        intent.putExtra("user", loggedUser);

        startActivity(intent);
    }

    @Override
    public void onVaControlSelected(VerbalAutopsyControl deadIndividual) {
        Intent intent = new Intent(this, IndividualDetailsActivity.class);
        intent.putExtra("individual", deadIndividual);
        intent.putExtra("user", loggedUser);

        startActivity(intent);
    }

    class IndividualSearchTask extends AsyncTask<Void, Void, IndividualArrayAdapter> {
        private String name;
        private String code;
        private String gender;
        private String houseNr;
        private Integer minAge;
        private Integer maxAge;

        public IndividualSearchTask(String name, String code, String houseNumber, String gender, Integer minAge, Integer maxAge) {
            this.name = name;
            this.code = code;
            this.houseNr = houseNumber;
            this.gender = gender;
            this.minAge = minAge;
            this.maxAge = maxAge;
        }

        @Override
        protected IndividualArrayAdapter doInBackground(Void... params) {
            return individualListFragment.loadIndividualsByFilters(name, code, houseNr, gender, minAge, maxAge);
        }

        @Override
        protected void onPostExecute(IndividualArrayAdapter adapter) {
            individualListFragment.setAdapter(adapter);
            individualListFragment.showProgress(false);
        }
    }

}
