package net.openva.mobile.main;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import net.openva.mobile.R;
import net.openva.mobile.database.*;
import net.openva.mobile.io.*;
import net.openva.mobile.model.*;

import java.util.List;

import mz.betainteractive.utilities.StringUtil;

public class ServerSyncActivity extends Activity implements SyncDatabaseListener {

    private Button btSyncUsers;
    private Button btSyncIndividuals;
    private Button btSyncVaControl;
    private Button btSyncVaStats;

    private TextView txtSyncUsersStatus;
    private TextView txtSyncIndividualsStatus;
    private TextView txtSyncVaControlStatus;
    private TextView txtSyncVaStatsStatus;

    private ProgressDialog progressDialog;

    private String serverUrl;
    private String username;
    private String password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.server_sync);

        initialize();
        showStatus();
    }

    @Override
    public void collectionComplete(String result) {
        showStatus();
    }

    private void showStatus() {
        Database db = new Database(this);

        db.open();

        List<SyncReport> reports = Queries.getAllSyncReportBy(db, null, null);

        for (SyncReport report : reports){
            String status = getString(R.string.server_sync_status_notsynced_lbl);

            if (report.getStatus()==SyncReport.STATUS_SYNCED){
                status = getString(R.string.server_sync_status_synced_lbl) + " " + StringUtil.format(report.getDate(), "yyyy-MM-dd HH:mm:ss");
            }

            if (report.getStatus()==SyncReport.STATUS_SYNC_ERROR){
                status = getString(R.string.server_sync_status_sync_error_lbl) + " " + StringUtil.format(report.getDate(), "yyyy-MM-dd HH:mm:ss");
            }

            switch (report.getReportId()){
                case SyncReport.REPORT_USERS: txtSyncUsersStatus.setText(status+"");  break;
                case SyncReport.REPORT_INDIVIDUALS: txtSyncIndividualsStatus.setText(status+"");  break;
                case SyncReport.REPORT_VA_CONTROLS: txtSyncVaControlStatus.setText(status+"");  break;
                case SyncReport.REPORT_VA_STATS: txtSyncVaStatsStatus.setText(status+"");  break;
            }
        }

        db.close();
    }

    private void initialize() {

        this.btSyncIndividuals = (Button) findViewById(R.id.btSyncIndividuals);
        this.btSyncUsers = (Button) findViewById(R.id.btSyncUsers);
        this.btSyncVaControl = (Button) findViewById(R.id.btSyncVaControl);
        this.btSyncVaStats = (Button) findViewById(R.id.btSyncVaStats);

        this.txtSyncIndividualsStatus = (TextView) findViewById(R.id.txtSyncIndividualsStatus);
        this.txtSyncUsersStatus = (TextView) findViewById(R.id.txtSyncUsersStatus);
        this.txtSyncVaControlStatus = (TextView) findViewById(R.id.txtSyncVaControlStatus);
        this.txtSyncVaStatsStatus = (TextView) findViewById(R.id.txtSyncVaStatsStatus);

        this.progressDialog = new ProgressDialog(this);

        this.btSyncIndividuals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                syncIndividuals();
            }
        });

        this.btSyncUsers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                syncUsers();
            }
        });

        this.btSyncVaControl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                syncVaControls();
            }
        });

        this.btSyncVaStats.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                syncVaStats();
            }
        });

        this.serverUrl = Queries.getApplicationParamValue(ApplicationParam.OPENVA_URL, this);
        this.username = (String) getIntent().getExtras().get("username");
        this.password = (String) getIntent().getExtras().get("password");


        this.progressDialog.setCancelable(false);
    }

    private void syncIndividuals() {
        SyncEntitiesTask syncEntitiesTask = new SyncEntitiesTask(this, progressDialog, this, serverUrl, username, password, SyncEntitiesTask.Entity.INDIVIDUALS);
        syncEntitiesTask.execute();
    }

    private void syncUsers() {
        SyncEntitiesTask syncEntitiesTask = new SyncEntitiesTask(this, progressDialog, this, serverUrl, username, password, SyncEntitiesTask.Entity.USERS);
        syncEntitiesTask.execute();
    }

    private void syncVaControls() {
        SyncEntitiesTask syncEntitiesTask = new SyncEntitiesTask(this, progressDialog, this, serverUrl, username, password, SyncEntitiesTask.Entity.VA_CONTROLS);
        syncEntitiesTask.execute();
    }

    private void syncVaStats() {
        SyncEntitiesTask syncEntitiesTask = new SyncEntitiesTask(this, progressDialog, this, serverUrl, username, password, SyncEntitiesTask.Entity.VA_STATS);
        syncEntitiesTask.execute();
    }
}
