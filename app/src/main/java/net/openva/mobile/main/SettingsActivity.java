package net.openva.mobile.main;

import android.app.Activity;
import android.os.Bundle;
import android.preference.EditTextPreference;

import net.openva.mobile.R;
import net.openva.mobile.fragment.SettingsFragment;

public class SettingsActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings);

        this.setTitle("Settings");
    }

}
