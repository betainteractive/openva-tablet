package net.openva.mobile.model;

import android.content.ContentValues;

import net.openva.mobile.database.DatabaseHelper;
import net.openva.mobile.database.Table;

import java.io.Serializable;
import mz.betainteractive.utilities.ReflectionUtils;

/**
 * This class represents a Authenticable User that are registered on the server
 */
public class User implements Serializable, Table {

    private int id;
    private String firstName;
    private String lastName;
    private String fullName;
    private String username;
    private String password;
    private String roles;
    private int totalCollected;
    private int totalAttempts;

    public User(){

    }

    public User(String firstName, String lastName, String fullName, String username, String password, String roles, int tCollected, int tAttempts) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.fullName = fullName;
        this.username = username;
        this.password = password;
        this.roles = roles;
        this.totalCollected = tCollected;
        this.totalAttempts = tAttempts;
    }

    @Override
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public void setFullName() {
        this.fullName = this.firstName + ((lastName==null||lastName.isEmpty()) ? "" : " "+lastName );
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRoles() {
        return roles;
    }

    public void setRoles(String roles) {
        this.roles = roles;
    }

    public int getTotalCollected() {
        return totalCollected;
    }

    public void setTotalCollected(int totalCollected) {
        this.totalCollected = totalCollected;
    }

    public int getTotalAttempts() {
        return totalAttempts;
    }

    public void setTotalAttempts(int totalAttempts) {
        this.totalAttempts = totalAttempts;
    }

    public String toString(){
        return ""+this.firstName+" "+this.lastName+", user: "+this.username;
    }

    public String getValueByName(String variableName){
        return ReflectionUtils.getValueByName(this, variableName);
    }

    @Override
    public String getTableName() {
        return DatabaseHelper.User.TABLE_NAME;
    }

    @Override
    public ContentValues getContentValues() {
        ContentValues cv = new ContentValues();
        cv.put(DatabaseHelper.User.COLUMN_FIRSTNAME, firstName);
        cv.put(DatabaseHelper.User.COLUMN_LASTNAME, lastName);
        cv.put(DatabaseHelper.User.COLUMN_USERNAME, username);
        cv.put(DatabaseHelper.User.COLUMN_FULLNAME, fullName);
        cv.put(DatabaseHelper.User.COLUMN_PASSWORD, password);
        cv.put(DatabaseHelper.User.COLUMN_ROLES, roles);
        cv.put(DatabaseHelper.User.COLUMN_TOTAL_COLLECTED, totalCollected);
        cv.put(DatabaseHelper.User.COLUMN_TOTAL_ATTEMPTS, totalAttempts);
        return cv;
    }

    @Override
    public String[] getColumnNames() {
        return DatabaseHelper.User.ALL_COLUMNS;
    }
}
