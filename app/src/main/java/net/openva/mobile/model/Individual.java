package net.openva.mobile.model;

import android.content.ContentValues;

import net.openva.mobile.database.DatabaseHelper;
import net.openva.mobile.database.Table;

import java.io.Serializable;
import java.util.Date;

import mz.betainteractive.utilities.StringUtil;

/**
 * This class represents a live individual (the server says is alive but could be dead)
 */
public class Individual implements Serializable, Table {

    private int id;
    private String extId;             /** Individual Id from OpenHDS **/
    private String code;              /** Individual identification code (HDSS site specific code) - can be the same as extId **/
    private String name;              /** FullName of the individual **/
    private String gender;            /** Gender of the dead individual **/
    private String dateOfBirth;         /** Date of Birth**/
    private int age;                  /** Age  **/

    private String motherId;          /** Individual Id of the mother from OpenHDS**/
    private String motherName;        /** mother's name **/
    private String fatherId;          /** Individual Id of the father from OpenHDS**/
    private String fatherName;        /** mother's name **/

    private String householdId;       /** Location Id from OpenHDS **/
    private String householdNo;       /** Household Number - it can be the Location.locationName from OpenHDS **/

    private boolean gpsAvailable = true;
    private Double gpsAccuracy;       /** gps accuracy data can be blank or zero**/
    private Double gpsAltitude;       /** gps altitude data can be blank or zero **/
    private Double gpsLatitude;       /** gps latitude data can be blank or zero **/
    private Double gpsLongitude;      /** gps longitude data can be blank  or zero**/

    /* the variables below are used to calculate distance between two houses directly via sql*/
    private Double cosLatitude;
    private Double sinLatitude;
    private Double cosLongitude;
    private Double sinLongitude;


    public Individual(){

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getExtId() {
        return extId;
    }

    public void setExtId(String extId) {
        this.extId = extId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public Date getDobDate(){
        return StringUtil.toDate(dateOfBirth, "yyyy-MM-dd");
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getMotherId() {
        return motherId;
    }

    public void setMotherId(String motherId) {
        this.motherId = motherId;
    }

    public String getMotherName() {
        return motherName;
    }

    public void setMotherName(String motherName) {
        this.motherName = motherName;
    }

    public String getFatherId() {
        return fatherId;
    }

    public void setFatherId(String fatherId) {
        this.fatherId = fatherId;
    }

    public String getFatherName() {
        return fatherName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    public String getHouseholdId() {
        return householdId;
    }

    public void setHouseholdId(String householdId) {
        this.householdId = householdId;
    }

    public String getHouseholdNo() {
        return householdNo;
    }

    public void setHouseholdNo(String householdNo) {
        this.householdNo = householdNo;
    }

    public boolean isGpsAvailable() {
        return gpsAvailable;
    }

    public void setGpsAvailable(boolean gpsAvailable) {
        this.gpsAvailable = gpsAvailable;
    }

    public Double getGpsAccuracy() {
        return gpsAccuracy;
    }

    public void setGpsAccuracy(Double gpsAccuracy) {
        this.gpsAccuracy = gpsAccuracy;
    }

    public Double getGpsAltitude() {
        return gpsAltitude;
    }

    public void setGpsAltitude(Double gpsAltitude) {
        this.gpsAltitude = gpsAltitude;
    }

    public Double getGpsLatitude() {
        return gpsLatitude;
    }

    public void setGpsLatitude(Double gpsLatitude) {
        this.gpsLatitude = gpsLatitude;
    }

    public Double getGpsLongitude() {
        return gpsLongitude;
    }

    public void setGpsLongitude(Double gpsLongitude) {
        this.gpsLongitude = gpsLongitude;
    }

    public Double getCosLatitude() {
        return cosLatitude;
    }

    public void setCosLatitude(Double cosLatitude) {
        this.cosLatitude = cosLatitude;
    }

    public Double getSinLatitude() {
        return sinLatitude;
    }

    public void setSinLatitude(Double sinLatitude) {
        this.sinLatitude = sinLatitude;
    }

    public Double getCosLongitude() {
        return cosLongitude;
    }

    public void setCosLongitude(Double cosLongitude) {
        this.cosLongitude = cosLongitude;
    }

    public Double getSinLongitude() {
        return sinLongitude;
    }

    public void setSinLongitude(Double sinLongitude) {
        this.sinLongitude = sinLongitude;
    }

    public static Individual getEmptyIndividual(){
        Individual individual = new Individual();
        individual.extId = "";
        individual.code = "";
        individual.name = "";
        individual.gender = "";
        individual.dateOfBirth = "";
        individual.age = -1;

        individual.motherId = "";
        individual.motherName = "";
        individual.fatherId = "";
        individual.fatherName = "";

        individual.householdId = "";
        individual.householdNo = "";

        individual.gpsAvailable = false;
        individual.gpsAccuracy = 0.0;
        individual.gpsAltitude = 0.0;
        individual.gpsLatitude = 0.0;
        individual.gpsLongitude = 0.0;

        individual.cosLatitude = 0.0;
        individual.sinLatitude = 0.0;
        individual.cosLongitude = 0.0;
        individual.sinLongitude = 0.0;

        return individual;
    }

    @Override
    public String getTableName() {
        return DatabaseHelper.Individual.TABLE_NAME;
    }

    @Override
    public ContentValues getContentValues() {
        ContentValues cv = new ContentValues();
        cv.put(DatabaseHelper.Individual.COLUMN_EXT_ID, extId);
        cv.put(DatabaseHelper.Individual.COLUMN_CODE, code);
        cv.put(DatabaseHelper.Individual.COLUMN_NAME, name);
        cv.put(DatabaseHelper.Individual.COLUMN_GENDER, gender);
        cv.put(DatabaseHelper.Individual.COLUMN_DATE_OF_BIRTH, dateOfBirth);
        cv.put(DatabaseHelper.Individual.COLUMN_AGE, age);

        cv.put(DatabaseHelper.Individual.COLUMN_MOTHER_ID, motherId);
        cv.put(DatabaseHelper.Individual.COLUMN_MOTHER_NAME, motherName);
        cv.put(DatabaseHelper.Individual.COLUMN_FATHER_ID, fatherId);
        cv.put(DatabaseHelper.Individual.COLUMN_FATHER_NAME, fatherName);

        cv.put(DatabaseHelper.Individual.COLUMN_HOUSEHOLD_ID, householdId);
        cv.put(DatabaseHelper.Individual.COLUMN_HOUSEHOLD_NO, householdNo);

        cv.put(DatabaseHelper.Individual.COLUMN_GPS_AVAILABLE, gpsAvailable ? 1 : 0);
        cv.put(DatabaseHelper.Individual.COLUMN_GPS_ACCURACY, gpsAccuracy);
        cv.put(DatabaseHelper.Individual.COLUMN_GPS_ALTITUDE, gpsAltitude);
        cv.put(DatabaseHelper.Individual.COLUMN_GPS_LATITUDE, gpsLatitude);
        cv.put(DatabaseHelper.Individual.COLUMN_GPS_LONGITUDE, gpsLongitude);

        cv.put(DatabaseHelper.Individual.COLUMN_COS_LATITUDE, cosLatitude);
        cv.put(DatabaseHelper.Individual.COLUMN_SIN_LATITUDE, sinLatitude);
        cv.put(DatabaseHelper.Individual.COLUMN_COS_LONGITUDE, cosLongitude);
        cv.put(DatabaseHelper.Individual.COLUMN_SIN_LONGITUDE, sinLongitude);
        return cv;
    }

    @Override
    public String[] getColumnNames() {
        return DatabaseHelper.Individual.ALL_COLUMNS;
    }
}
