package net.openva.mobile.model;

import android.content.ContentValues;

import net.openva.mobile.database.DatabaseHelper;
import net.openva.mobile.database.Table;

import java.io.Serializable;

/**
 * The Stats of Verbal Autopsy Collection
 */
public class VerbalAutopsyStats implements Serializable, Table {

    private int id;
    private String creationDate; /* The Date when the stats were calculated */
    private int totalDeaths;     /* Total of Death Registered - VerbalAutopsyControl */
    private int totalToCollect;  /* Total of Verbal Autopsy that need to be collected */
    private int totalCollected;  /* Total of Verbal Autopsy collected */
    private int totalAttempts;   /* Total of attempts to collect VA - represents the number of rejections (not consented + impossible to visit)*/
    private int totalAttemptNi;  /* Total of attempts with reason equals to "No Informant" */
    private int totalAttemptUh;  /* Total of attempts with reason equals to "Uninhabited House/House destroyed" */
    private int totalAttemptWdr; /* Total of attempts with reason equals to "Wrong Death Record" */
    private int totalAttemptOt;  /* Total of attempts with reason equals to "Other Reason" */
    private int totalAttemptNc;  /* Total of attempts with reason equals to "Not Consented" */

    public VerbalAutopsyStats(){

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public int getTotalDeaths() {
        return totalDeaths;
    }

    public void setTotalDeaths(int totalDeaths) {
        this.totalDeaths = totalDeaths;
    }

    public int getTotalToCollect() {
        return totalToCollect;
    }

    public void setTotalToCollect(int totalToCollect) {
        this.totalToCollect = totalToCollect;
    }

    public int getTotalCollected() {
        return totalCollected;
    }

    public void setTotalCollected(int totalCollected) {
        this.totalCollected = totalCollected;
    }

    public int getTotalAttempts() {
        return totalAttempts;
    }

    public void setTotalAttempts(int totalAttempts) {
        this.totalAttempts = totalAttempts;
    }

    public int getTotalAttemptNi() {
        return totalAttemptNi;
    }

    public void setTotalAttemptNi(int totalAttemptNi) {
        this.totalAttemptNi = totalAttemptNi;
    }

    public int getTotalAttemptUh() {
        return totalAttemptUh;
    }

    public void setTotalAttemptUh(int totalAttemptUh) {
        this.totalAttemptUh = totalAttemptUh;
    }

    public int getTotalAttemptWdr() {
        return totalAttemptWdr;
    }

    public void setTotalAttemptWdr(int totalAttemptWdr) {
        this.totalAttemptWdr = totalAttemptWdr;
    }

    public int getTotalAttemptOt() {
        return totalAttemptOt;
    }

    public void setTotalAttemptOt(int totalAttemptOt) {
        this.totalAttemptOt = totalAttemptOt;
    }

    public int getTotalAttemptNc() {
        return totalAttemptNc;
    }

    public void setTotalAttemptNc(int totalAttemptNc) {
        this.totalAttemptNc = totalAttemptNc;
    }

    @Override
    public String getTableName() {
        return DatabaseHelper.VerbalAutopsyStats.TABLE_NAME;
    }

    @Override
    public ContentValues getContentValues() {
        ContentValues cv = new ContentValues();

        cv.put(DatabaseHelper.VerbalAutopsyStats.COLUMN_CREATION_DATE, creationDate);
        cv.put(DatabaseHelper.VerbalAutopsyStats.COLUMN_TOTAL_DEATHS, totalDeaths);
        cv.put(DatabaseHelper.VerbalAutopsyStats.COLUMN_TOTAL_TO_COLLECT, totalToCollect);
        cv.put(DatabaseHelper.VerbalAutopsyStats.COLUMN_TOTAL_COLLECTED, totalCollected);
        cv.put(DatabaseHelper.VerbalAutopsyStats.COLUMN_TOTAL_ATTEMPTS, totalAttempts);
        cv.put(DatabaseHelper.VerbalAutopsyStats.COLUMN_TOTAL_ATTEMPT_NI, totalAttemptNi);
        cv.put(DatabaseHelper.VerbalAutopsyStats.COLUMN_TOTAL_ATTEMPT_UH, totalAttemptUh);
        cv.put(DatabaseHelper.VerbalAutopsyStats.COLUMN_TOTAL_ATTEMPT_WDR, totalAttemptWdr);
        cv.put(DatabaseHelper.VerbalAutopsyStats.COLUMN_TOTAL_ATTEMPT_OT, totalAttemptOt);
        cv.put(DatabaseHelper.VerbalAutopsyStats.COLUMN_TOTAL_ATTEMPT_NC, totalAttemptNc);

        return cv;
    }

    @Override
    public String[] getColumnNames() {
        return DatabaseHelper.VerbalAutopsyStats.ALL_COLUMNS;
    }
}
