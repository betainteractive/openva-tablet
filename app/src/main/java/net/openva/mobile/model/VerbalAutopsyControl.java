package net.openva.mobile.model;

import android.content.ContentValues;

import net.openva.mobile.database.DatabaseHelper;
import net.openva.mobile.database.Table;

import java.io.Serializable;

/**
 * Verbal Autopsy Control Table - also it is a death registration like table
 */
public class VerbalAutopsyControl implements Serializable, Table {

    private int id;
    private String extId;             /** Individual Id from OpenHDS **/
    private String code;              /** Individual identification code (HDSS site specific code) - can be the same as extId **/
    private String name;              /** FullName of the individual **/
    private String gender;            /** Gender of the dead individual **/
    private String dateOfBirth;       /** Date of Birth**/
    private String dateOfDeath;       /** Date of Death **/
    private int ageAtDeath;           /** Age of the individual at death **/

    private String motherId;          /** Individual Id of the mother from OpenHDS**/
    private String motherName;        /** mother's name **/
    private String fatherId;          /** Individual Id of the father from OpenHDS**/
    private String fatherName;        /** mother's name **/

    private String householdId;       /** Location Id from OpenHDS **/
    private String householdNo;       /** Household Number - it can be the Location.locationName from OpenHDS **/

    private boolean gpsAvailable;
    private Double gpsAccuracy;       /** gps accuracy data can be blank or zero**/
    private Double gpsAltitude;       /** gps altitude data can be blank or zero **/
    private Double gpsLatitude;       /** gps latitude data can be blank or zero **/
    private Double gpsLongitude;      /** gps longitude data can be blank  or zero**/

    private Integer vaType;           /** verbal autopsy type - neonatal, adult or maternal **/
    private Integer vaCollected;      /** represent the va collection status - 0-not_collected, 1-collected, 2-collected-attempt (can be send to tablet for collection) **/
    private String  vaProcessedWith;  /** which tool was used to calculate the causes of death interVa/insilicoVa/smartVa **/
    private Integer vaProcessed;      /** represent the status of calculation of death causes using interVa/insilicoVa - 0: not calculated, 1: calculated **/
    private String  vaResult;         /** the causes of death calculated by va tools (insilico,interva,smartva) **/

    private Integer vaAttempts;        /** Number of attempts to collect VA's (not completed forms by some reason <eg. household destroyed>)  **/
    private String lastAttemptDate;
    private String lastAttemptReason;

    /* the variables below are used to calculate distance between two houses directly via sql*/
    private Double cosLatitude;
    private Double sinLatitude;
    private Double cosLongitude;
    private Double sinLongitude;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getExtId() {
        return extId;
    }

    public void setExtId(String extId) {
        this.extId = extId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getDateOfDeath() {
        return dateOfDeath;
    }

    public void setDateOfDeath(String dateOfDeath) {
        this.dateOfDeath = dateOfDeath;
    }

    public int getAgeAtDeath() {
        return ageAtDeath;
    }

    public void setAgeAtDeath(int ageAtDeath) {
        this.ageAtDeath = ageAtDeath;
    }

    public String getMotherId() {
        return motherId;
    }

    public void setMotherId(String motherId) {
        this.motherId = motherId;
    }

    public String getMotherName() {
        return motherName;
    }

    public void setMotherName(String motherName) {
        this.motherName = motherName;
    }

    public String getFatherId() {
        return fatherId;
    }

    public void setFatherId(String fatherId) {
        this.fatherId = fatherId;
    }

    public String getFatherName() {
        return fatherName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    public String getHouseholdId() {
        return householdId;
    }

    public void setHouseholdId(String householdId) {
        this.householdId = householdId;
    }

    public String getHouseholdNo() {
        return householdNo;
    }

    public void setHouseholdNo(String householdNo) {
        this.householdNo = householdNo;
    }

    public boolean isGpsAvailable() {
        return gpsAvailable;
    }

    public void setGpsAvailable(boolean gpsAvailable) {
        this.gpsAvailable = gpsAvailable;
    }

    public Double getGpsAccuracy() {
        return gpsAccuracy;
    }

    public void setGpsAccuracy(Double gpsAccuracy) {
        this.gpsAccuracy = gpsAccuracy;
    }

    public Double getGpsAltitude() {
        return gpsAltitude;
    }

    public void setGpsAltitude(Double gpsAltitude) {
        this.gpsAltitude = gpsAltitude;
    }

    public Double getGpsLatitude() {
        return gpsLatitude;
    }

    public void setGpsLatitude(Double gpsLatitude) {
        this.gpsLatitude = gpsLatitude;
    }

    public Double getGpsLongitude() {
        return gpsLongitude;
    }

    public void setGpsLongitude(Double gpsLongitude) {
        this.gpsLongitude = gpsLongitude;
    }

    public Integer getVaType() {
        return vaType;
    }

    public void setVaType(Integer vaType) {
        this.vaType = vaType;
    }

    public Integer getVaCollected() {
        return vaCollected;
    }

    public void setVaCollected(Integer vaCollected) {
        this.vaCollected = vaCollected;
    }

    public String getVaProcessedWith() {
        return vaProcessedWith;
    }

    public void setVaProcessedWith(String vaProcessedWith) {
        this.vaProcessedWith = vaProcessedWith;
    }

    public Integer getVaProcessed() {
        return vaProcessed;
    }

    public void setVaProcessed(Integer vaProcessed) {
        this.vaProcessed = vaProcessed;
    }

    public String getVaResult() {
        return vaResult;
    }

    public void setVaResult(String vaResult) {
        this.vaResult = vaResult;
    }

    public Integer getVaAttempts() {
        return vaAttempts;
    }

    public void setVaAttempts(Integer vaAttempts) {
        this.vaAttempts = vaAttempts;
    }

    public String getLastAttemptDate() {
        return lastAttemptDate;
    }

    public void setLastAttemptDate(String lastAttemptDate) {
        this.lastAttemptDate = lastAttemptDate;
    }

    public String getLastAttemptReason() {
        return lastAttemptReason;
    }

    public void setLastAttemptReason(String lastAttemptReason) {
        this.lastAttemptReason = lastAttemptReason;
    }

    public Double getCosLatitude() {
        return cosLatitude;
    }

    public void setCosLatitude(Double cosLatitude) {
        this.cosLatitude = cosLatitude;
    }

    public Double getSinLatitude() {
        return sinLatitude;
    }

    public void setSinLatitude(Double sinLatitude) {
        this.sinLatitude = sinLatitude;
    }

    public Double getCosLongitude() {
        return cosLongitude;
    }

    public void setCosLongitude(Double cosLongitude) {
        this.cosLongitude = cosLongitude;
    }

    public Double getSinLongitude() {
        return sinLongitude;
    }

    public void setSinLongitude(Double sinLongitude) {
        this.sinLongitude = sinLongitude;
    }

    public static VerbalAutopsyControl getEmptyVaControl(){
        VerbalAutopsyControl vaControl = new VerbalAutopsyControl();

        vaControl.extId = "";
        vaControl.code = "";
        vaControl.name = "";
        vaControl.gender = "";
        vaControl.dateOfBirth = "";
        vaControl.dateOfDeath = "";
        vaControl.ageAtDeath = -1;

        vaControl.motherId = "";
        vaControl.motherName = "";
        vaControl.fatherId = "";
        vaControl.fatherName = "";

        vaControl.householdId = "";
        vaControl.householdNo = "";

        vaControl.gpsAvailable = false;
        vaControl.gpsAccuracy = 0.0;
        vaControl.gpsAltitude = 0.0;
        vaControl.gpsLatitude = 0.0;
        vaControl.gpsLongitude = 0.0;

        vaControl.vaType = 0;
        vaControl.vaCollected = 0;
        vaControl.vaProcessedWith = "";
        vaControl.vaProcessed = 0;
        vaControl.vaResult = "";

        vaControl.vaAttempts = 0;
        vaControl.lastAttemptDate = "";
        vaControl.lastAttemptReason = "";

        vaControl.cosLatitude = 0.0;
        vaControl.sinLatitude = 0.0;
        vaControl.cosLongitude = 0.0;
        vaControl.sinLongitude = 0.0;

        return vaControl;
    }

    @Override
    public String getTableName() {
        return DatabaseHelper.VerbalAutopsyControl.TABLE_NAME;
    }

    @Override
    public ContentValues getContentValues() {
        ContentValues cv = new ContentValues();

        cv.put(DatabaseHelper.VerbalAutopsyControl.COLUMN_EXT_ID, extId);
        cv.put(DatabaseHelper.VerbalAutopsyControl.COLUMN_CODE, code);
        cv.put(DatabaseHelper.VerbalAutopsyControl.COLUMN_NAME, name);
        cv.put(DatabaseHelper.VerbalAutopsyControl.COLUMN_GENDER, gender);
        cv.put(DatabaseHelper.VerbalAutopsyControl.COLUMN_DATE_OF_BIRTH, dateOfBirth);
        cv.put(DatabaseHelper.VerbalAutopsyControl.COLUMN_DATE_OF_DEATH, dateOfDeath);
        cv.put(DatabaseHelper.VerbalAutopsyControl.COLUMN_AGE_AT_DEATH, ageAtDeath);

        cv.put(DatabaseHelper.VerbalAutopsyControl.COLUMN_MOTHER_ID, motherId);
        cv.put(DatabaseHelper.VerbalAutopsyControl.COLUMN_MOTHER_NAME, motherName);
        cv.put(DatabaseHelper.VerbalAutopsyControl.COLUMN_FATHER_ID, fatherId);
        cv.put(DatabaseHelper.VerbalAutopsyControl.COLUMN_FATHER_NAME, fatherName);

        cv.put(DatabaseHelper.VerbalAutopsyControl.COLUMN_HOUSEHOLD_ID, householdId);
        cv.put(DatabaseHelper.VerbalAutopsyControl.COLUMN_HOUSEHOLD_NO, householdNo);

        cv.put(DatabaseHelper.VerbalAutopsyControl.COLUMN_GPS_AVAILABLE, gpsAvailable ? 1 : 0);
        cv.put(DatabaseHelper.VerbalAutopsyControl.COLUMN_GPS_ACCURACY, gpsAccuracy);
        cv.put(DatabaseHelper.VerbalAutopsyControl.COLUMN_GPS_ALTITUDE, gpsAltitude);
        cv.put(DatabaseHelper.VerbalAutopsyControl.COLUMN_GPS_LATITUDE, gpsLatitude);
        cv.put(DatabaseHelper.VerbalAutopsyControl.COLUMN_GPS_LONGITUDE, gpsLongitude);

        cv.put(DatabaseHelper.VerbalAutopsyControl.COLUMN_VA_TYPE, vaType);
        cv.put(DatabaseHelper.VerbalAutopsyControl.COLUMN_VA_COLLECTED, vaCollected);
        cv.put(DatabaseHelper.VerbalAutopsyControl.COLUMN_VA_PROCESSED_WITH, vaProcessedWith);
        cv.put(DatabaseHelper.VerbalAutopsyControl.COLUMN_VA_PROCESSED, vaProcessed);
        cv.put(DatabaseHelper.VerbalAutopsyControl.COLUMN_VA_RESULT, vaResult);
        cv.put(DatabaseHelper.VerbalAutopsyControl.COLUMN_VA_ATTEMPTS, vaAttempts);

        cv.put(DatabaseHelper.VerbalAutopsyControl.COLUMN_LAST_ATTEMPT_DATE, lastAttemptDate);
        cv.put(DatabaseHelper.VerbalAutopsyControl.COLUMN_LAST_ATTEMPT_REASON, lastAttemptReason);

        return cv;
    }

    @Override
    public String[] getColumnNames() {
        return DatabaseHelper.VerbalAutopsyControl.ALL_COLUMNS;
    }
}
