package net.openva.mobile.io.xml;

import android.util.Log;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import mz.betainteractive.utilities.StringUtil;

/**
 * Created by paul on 8/12/16.
 */
public class FormXmlReader {
    private XPath xpath = XPathFactory.newInstance().newXPath();
    private DocumentBuilder builder;

    private Document buildDocument(InputStream is) throws ParserConfigurationException, SAXException, IOException {
        if (builder == null) {
            builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        }

        Document doc = builder.parse(is);
        return doc;
    }

    public String readDeathDateFromWhoVA(FileInputStream fileInputStream, String jrFormId)  {
        try {
            Document doc = buildDocument(fileInputStream);
            if(xpath.evaluate("/"+jrFormId+"/isPreRegistered/text()", doc).length()==0) {
                jrFormId ="data";
            }

            String indi_code_path  = "/"+jrFormId+"/individualId/text()";
            String first_name_path = "/"+jrFormId+"/consented/deceased_CRVS/info_on_deceased/Id10017/text()";
            String last_name_path  = "/"+jrFormId+"/consented/deceased_CRVS/info_on_deceased/Id10018/text()";
            String death_date_path = "/"+jrFormId+"/consented/deceased_CRVS/info_on_deceased/Id10023/text()";

            String code = "";
            String name = "";
            String fname = "";
            String lname = "";
            String dthdate = "";

            if(xpath.evaluate(indi_code_path, doc).length()>0) {
                code = xpath.evaluate(indi_code_path, doc);
            }

            if(xpath.evaluate(first_name_path, doc).length()>0) {
                fname = xpath.evaluate(first_name_path, doc);
            }

            if(xpath.evaluate(last_name_path, doc).length()>0) {
                lname = xpath.evaluate(last_name_path, doc);
            }

            if(xpath.evaluate(death_date_path, doc).length()>0) {
                dthdate = xpath.evaluate(death_date_path, doc);
            }

            name = StringUtil.getFullname(fname, lname);

            Log.d("name", ""+name+"");
            Log.d("id", ""+code+"");
            Log.d("dthdate", ""+dthdate+"");

            return dthdate;

        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        }

        return null;
    }

}
