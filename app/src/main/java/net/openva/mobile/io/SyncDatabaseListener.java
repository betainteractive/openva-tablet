package net.openva.mobile.io;

public interface SyncDatabaseListener {
	void collectionComplete(String result);
}
