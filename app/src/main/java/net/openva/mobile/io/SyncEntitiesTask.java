package net.openva.mobile.io;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import net.openva.mobile.R;
import net.openva.mobile.database.*;
import net.openva.mobile.model.*;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import mz.betainteractive.utilities.StringUtil;

/**
 * AsyncTask responsible for downloading the OpenVA "database", that is a
 * subset of the OpenVA database records.
 * Its inspired by OpenHDS Tablet SyncEntitiesTask class
 */
public class SyncEntitiesTask extends AsyncTask<Void, Integer, String> {

	private static final String API_PATH = "/api/export";
	private static final String ZIP_MIME_TYPE = "application/zip;charset=utf-8";
	private static final String XML_MIME_TYPE = "text/xml;charset=utf-8";

	private SyncDatabaseListener listener;

	private ProgressDialog dialog;
	private HttpURLConnection connection;

	private String baseurl;
	private String username;
	private String password;
	private List<Entity> entities;

	private final List<Table> values = new ArrayList<Table>();

	private State state;
	private Entity entity;

	private Context mContext;

	private enum State {
		DOWNLOADING, SAVING
	}

	public enum Entity {
		USERS, INDIVIDUALS, VA_CONTROLS, VA_STATS
	}

	public SyncEntitiesTask(Context context, ProgressDialog dialog, SyncDatabaseListener listener, String url, String username, String password, Entity... entityToDownload) {
		this.baseurl = url;
		this.username = username;
		this.password = password;
		this.dialog = dialog;
		this.listener = listener;
		this.mContext = context;
		this.entities = new ArrayList<>();
		this.entities.addAll(Arrays.asList(entityToDownload));
		initDialog();
	}

	public SyncEntitiesTask(Context context, ProgressDialog dialog, String url, String username, String password, Entity... entityToDownload) {
		this.baseurl = url;
		this.username = username;
		this.password = password;
		this.dialog = dialog;
		this.mContext = context;
		this.entities = new ArrayList<>();
		this.entities.addAll(Arrays.asList(entityToDownload));
		initDialog();
	}

	public SyncEntitiesTask(Context context, ProgressDialog dialog, String url, String username, String password) {
		this.baseurl = url;
		this.username = username;
		this.password = password;
		this.dialog = dialog;
		this.mContext = context;
		this.entities = new ArrayList<>();
		initDialog();
	}

	private void initDialog(){
		dialog.setMessage(mContext.getString(R.string.sync_prepare_download_lbl));
		dialog.show();
	}

	private Database getDatabase(){
		return new Database(mContext);
	}

	public void setSyncDatabaseListener(SyncDatabaseListener listener){
		this.listener = listener;
	}

	public void setEntitiesToDownload(Entity... entityToDownload){
		this.entities.clear();
		this.entities.addAll(Arrays.asList(entityToDownload));
	}

	@Override
	protected void onProgressUpdate(Integer... values) {
		StringBuilder builder = new StringBuilder();

		switch (state) {
		case DOWNLOADING:
			builder.append(mContext.getString(R.string.sync_downloading_lbl));
			break;
		case SAVING:
			builder.append(mContext.getString(R.string.sync_saving_lbl));
			break;
		}

		switch (entity) {
			case USERS:
				builder.append(" " + mContext.getString(R.string.sync_users_lbl));
				break;
			case INDIVIDUALS:
				builder.append(" " + mContext.getString(R.string.sync_individuals_lbl));
				break;
			case VA_STATS:
				builder.append(" " + mContext.getString(R.string.sync_va_stats_lbl));
				break;
			case VA_CONTROLS:
				builder.append(" " + mContext.getString(R.string.sync_va_controls_lbl));
				break;
		}

		if (values.length > 0) {
			String msg = ". " + mContext.getString(R.string.sync_saved_lbl) + " "  + values[0] + " " + mContext.getString(R.string.sync_records_lbl);
			if (state== State.DOWNLOADING){
				msg = ". " + mContext.getString(R.string.sync_saved_lbl) + " "  + values[0] + "KB";
			}

			builder.append(msg);
		}

		dialog.setMessage(builder.toString());
	}

	protected String doInBackground(Void... params) {

		// at this point, we don't care to be smart about which data to
		// download, we simply download it all
		//deleteAllTables();

		try {

			for (Entity ent : entities){
				entity = ent;
				switch (entity) {
					case INDIVIDUALS:
						deleteAll(Individual.class);
						processUrl(baseurl + API_PATH + "/individuals/zip", "individuals.zip");
						break;
					case USERS:
						deleteAll(User.class);
						processUrl(baseurl + API_PATH + "/users/zip", "users.zip");
						break;
					case VA_CONTROLS:
						deleteAll(VerbalAutopsyControl.class);
						processUrl(baseurl + API_PATH + "/vacontrols/zip", "vacontrols.zip");
						break;
					case VA_STATS:
						deleteAll(VerbalAutopsyStats.class);
						//deleteAll(CollectedData.class, DatabaseHelper.CollectedData.COLUMN_SUPERVISED+"=1", null);
						processUrl(baseurl + API_PATH + "/stats/zip", "stats.zip");
						break;
				}
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
			//Toast.makeText(mContext, mContext.getString(R.string.sync_failure_file_not_found_lbl), Toast.LENGTH_LONG).show();
			return mContext.getString(R.string.sync_failure_file_not_found_lbl);//"Failure";//HttpTask.EndResult.FAILURE;
		} catch (Exception e) {
			e.printStackTrace();
			//Toast.makeText(mContext, mContext.getString(R.string.sync_failure_file_not_found_lbl), Toast.LENGTH_LONG).show();
			return mContext.getString(R.string.sync_failure_file_not_found_lbl);//"Failure";//HttpTask.EndResult.FAILURE;
		}


		return this.mContext.getString(R.string.sync_successfully_lbl);
	}

	private void deleteAllTables() {
		// ordering is somewhat important during delete. a few tables have
		// foreign keys
		Database database = getDatabase();
		database.open();

		database.delete(User.class, null, null);
		database.delete(Individual.class, null, null);
		database.delete(VerbalAutopsyControl.class, null, null);
		database.delete(VerbalAutopsyStats.class, null, null);
		//database.delete(CollectedData.class, DatabaseHelper.CollectedData.COLUMN_SUPERVISED+"=?", new String[]{ "1"}); //delete all collected data that was supervised

		database.close();
	}

	private void deleteAll(Class<? extends Table> table){
		Database database = getDatabase();
		database.open();
		database.delete(table, null, null);
		database.close();
	}

	private void deleteAll(Class<? extends Table>... tables){
		Database database = getDatabase();
		database.open();
		for (Class<? extends Table> table : tables){
			database.delete(table, null, null);
		}
		database.close();
	}

	private void deleteAll(Class<? extends Table> table, String whereClause, String[] whereClauseArgs){
		Database database = getDatabase();
		database.open();
		database.delete(table, whereClause, whereClauseArgs);
		database.close();
	}

	private void processUrl(String strUrl, String exportedFileName) throws Exception {
		state = State.DOWNLOADING;
		publishProgress();

		String basicAuth = "Basic " + new String(Base64.encode((this.username+":"+this.password).getBytes(),Base64.NO_WRAP ));

		URL url = new URL(strUrl);
		connection = (HttpURLConnection) url.openConnection();
		connection.setRequestMethod("GET");
		connection.setReadTimeout(10000);
		connection.setConnectTimeout(15000);
		connection.setDoInput(true);
		connection.setRequestProperty("Authorization", basicAuth);

		Log.d("processing", ""+url);

		connection.connect();

		processResponse(exportedFileName);
	}

	private void processResponse(String exportedFileName) throws Exception {
		DownloadResponse response = getResponse(exportedFileName);

		Log.d("is", ""+response.getInputStream()+", xml-"+response.isXmlFile()+", zip-"+response.isZipFile());

		//save file
		InputStream fileInputStream = saveFileToStorage(response);

		if (fileInputStream != null){
			if (response.isXmlFile()){
				processXMLDocument(fileInputStream);
			}
			if (response.isZipFile()){
				processZIPDocument(fileInputStream);
			}
		}
	}

	private DownloadResponse getResponse(String exportedFileName) throws IOException {
		int response = connection.getResponseCode();
		Log.d("connection", "The response code is: " + response+", type="+connection.getContentType()+", size="+connection.getContentLength());

		InputStream is = connection.getInputStream();

		return new DownloadResponse(is, connection.getContentType(), exportedFileName, connection.getContentLength());//173916816L);
	}

	private void processXMLDocument(InputStream content) throws Exception {
		state = State.SAVING;

		XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
		//factory.setNamespaceAware(true);
		XmlPullParser parser = factory.newPullParser();

		parser.setInput(content, null);

		int eventType = parser.getEventType();

		while (eventType != XmlPullParser.END_DOCUMENT && !isCancelled()) {
			String name = null;

			switch (eventType) {
			case XmlPullParser.START_TAG:
				name = parser.getName();
				if (name.equalsIgnoreCase("individuals")) {
					processIndividualsParams(parser);
				} else if (name.equalsIgnoreCase("users")) {
					processUsersParams(parser);
				} else if (name.equalsIgnoreCase("vacontrols")) {
					processVaControlsParams(parser);
				} else if (name.equalsIgnoreCase("vastats")) {
					processVaStatsParams(parser);
				}
				break;
			}

			eventType = parser.next();
		}
	}

	private InputStream saveFileToStorage(DownloadResponse response) throws Exception {

		InputStream content = response.getInputStream();

		FileOutputStream fout = new FileOutputStream(Bootstrap.getAppPath() + response.getFileName());
		byte[] buffer = new byte[10*1024];
		int len = 0;
		long total = 0;

		publishProgress();

		while ((len = content.read(buffer)) != -1){
			fout.write(buffer, 0, len);
			total += len;
			int perc =  (int) ((total/(1024)));
			publishProgress(perc);
		}

		fout.close();
		content.close();

		FileInputStream fin = new FileInputStream(Bootstrap.getAppPath() + response.getFileName());

		return fin;
	}

	private void processZIPDocument(InputStream inputStream) throws Exception {

		Log.d("zip", "processing zip file");


		ZipInputStream zin = new ZipInputStream(inputStream);
		ZipEntry entry = zin.getNextEntry();

		if (entry != null){
			processXMLDocument(zin);
			zin.closeEntry();
		}

		zin.close();
	}

	private boolean notEndOfXmlDoc(String element, XmlPullParser parser) throws XmlPullParserException {
		return !(element.equals(parser.getName()) && parser.getEventType() == XmlPullParser.END_TAG) && !isCancelled();
	}

	private boolean isEndTag(String element, XmlPullParser parser) throws XmlPullParserException {
		return (element.equals(parser.getName()) && parser.getEventType() == XmlPullParser.END_TAG);
	}

	private boolean isEmptyTag(String element, XmlPullParser parser) throws XmlPullParserException {
		return (element.equals(parser.getName()) && parser.isEmptyElementTag());
	}

	private void processUsersParams(XmlPullParser parser) throws XmlPullParserException, IOException {

		//clear sync_report
		updateSyncReport(SyncReport.REPORT_USERS, null, SyncReport.STATUS_NOT_SYNCED);

		int count = 0;
		values.clear();

		parser.nextTag();

		Database database = getDatabase();
		database.open();
		database.beginTransaction();

		while (notEndOfXmlDoc("users", parser)) {
			count++;

			User table = new User();

			parser.nextTag(); //process <username>
			if (!isEmptyTag("username", parser)) {
				parser.next();
				table.setUsername(parser.getText());
				parser.nextTag(); //process </username>
				//Log.d(count+"-username", "value="+ table.getUsername());
			}else{
				table.setUsername("");
				parser.nextTag();
			}

			parser.nextTag(); //process <password>
			if (!isEmptyTag("password", parser)) {
				parser.next();
				table.setPassword(parser.getText());
				parser.nextTag(); //process </password>
				//Log.d(count+"-password", "value="+ table.getPassword());
			}else{
				table.setPassword("");
				parser.nextTag();
			}

			parser.nextTag(); //process <firstName>
			if (!isEmptyTag("firstName", parser)) {
				parser.next();
				table.setFirstName(parser.getText());
				parser.nextTag(); //process </firstName>
				//Log.d(count+"-firstName", "value="+ table.getFirstName());
			}else{
				table.setFirstName("");
				parser.nextTag();
			}

			parser.nextTag(); //process <lastName>
			if (!isEmptyTag("lastName", parser)) { //its not <lastName/>
				parser.next();
				table.setLastName(parser.getText());
				parser.nextTag(); //process </lastName>
				//Log.d(count+"-lastName", "value="+ table.getLastName());
			}else{
				table.setLastName("");
				parser.nextTag();
			}

			table.setFullName();

			parser.nextTag(); //process <roles>
			if (!isEmptyTag("roles", parser)) {
				parser.next();
				table.setRoles(parser.getText());
				parser.nextTag(); //process </roles>
				//Log.d(count+"-roles", "value="+ table.getRoles());
			}else{
				table.setRoles("");
				parser.nextTag();
			}

			parser.nextTag(); //process <totalCollected>
			if (!isEmptyTag("totalCollected", parser)){ //its not <totalCollected/>
				parser.next();
				table.setTotalCollected(Integer.parseInt(parser.getText()));
				parser.nextTag(); // </totalCollected>
				//Log.d(count+"-totalCollected", "value="+ table.getTotalCollected());
			}else{
				table.setTotalCollected(0);
				parser.nextTag();
			}

			parser.nextTag(); //process <totalAttempts>
			if (!isEmptyTag("totalAttempts", parser)){ //its not <totalAttempts/>
				parser.next();
				table.setTotalAttempts(Integer.parseInt(parser.getText()));
				parser.nextTag(); // </totalAttempts>
				//Log.d(count+"-totalAttempts", "value="+ table.getTotalAttempts());
			}else{
				table.setTotalAttempts(0);
				parser.nextTag();
			}


			parser.nextTag(); // <user>
			parser.next();

			//values.add(table);
			database.insert(table);

			publishProgress(count);

		}

		/*
		state = State.SAVING;
		entity = Entity.USERS;

		Database database = getDatabase();
		database.open();
		if (!values.isEmpty()) {
			count = 0;
			for (Table t : values){
				count++;
				database.insert(t);
				publishProgress(count);
			}
		}
		*/

		database.setTransactionSuccessful();
		database.endTransaction();
		database.close();

		updateSyncReport(SyncReport.REPORT_USERS, new Date(), SyncReport.STATUS_SYNCED);
	}

	private void processIndividualsParams(XmlPullParser parser) throws XmlPullParserException, IOException {

		//clear sync_report
		updateSyncReport(SyncReport.REPORT_INDIVIDUALS, null, SyncReport.STATUS_NOT_SYNCED);

		int count = 0;
		values.clear();

		parser.nextTag();

		Database database = getDatabase();
		database.open();
		database.beginTransaction();

		while (notEndOfXmlDoc("individuals", parser)) {
			count++;

			Individual table = new Individual();

			//Log.d("TAG", parser.getPositionDescription());

			parser.nextTag(); //process <extId>
			//Log.d("TAG2", parser.getPositionDescription());
			if (!isEmptyTag(DatabaseHelper.Individual.COLUMN_EXT_ID, parser)) {
				parser.next();
				table.setExtId(parser.getText());
				parser.nextTag(); //process </extId>
			}else{
				table.setExtId("");
				parser.nextTag();
			}

			parser.nextTag(); //process <code>
			if (!isEmptyTag(DatabaseHelper.Individual.COLUMN_CODE, parser)) {
				parser.next();
				table.setCode(parser.getText());
				parser.nextTag(); //process </permId>
			}else{
				table.setCode("");
				parser.nextTag();
			}

			parser.nextTag(); //process <name>
			if (!isEmptyTag(DatabaseHelper.Individual.COLUMN_NAME, parser)) {
				parser.next();
				table.setName(parser.getText());
				parser.nextTag(); //process </name>
			}else{
				table.setName("");
				parser.nextTag();
			}

			parser.nextTag(); //process <gender>
			if (!isEmptyTag(DatabaseHelper.Individual.COLUMN_GENDER, parser)) {
				parser.next();
				table.setGender(parser.getText());
				parser.nextTag(); //process </gender>
			}else{
				table.setGender("");
				parser.nextTag();
			}

			parser.nextTag(); //process <dob>
			if (!isEmptyTag(DatabaseHelper.Individual.COLUMN_DATE_OF_BIRTH, parser)) {
				parser.next();
				table.setDateOfBirth(parser.getText());
				parser.nextTag(); //process </dob>
			}else{
				table.setDateOfBirth("");
				parser.nextTag();
			}

			parser.nextTag(); //process <age>
			if (!isEmptyTag(DatabaseHelper.Individual.COLUMN_AGE, parser)) {
				parser.next();
				table.setAge(Integer.parseInt(parser.getText()));
				parser.nextTag(); //process </age>
			}else{
				table.setAge(-1);
				parser.nextTag();
			}

			parser.nextTag(); //process <motherId>
			if (!isEmptyTag(DatabaseHelper.Individual.COLUMN_MOTHER_ID, parser)) {
				parser.next();
				table.setMotherId(parser.getText());
				parser.nextTag(); //process </motherId>
			}else{
				table.setMotherId("");
				parser.nextTag();
			}

			parser.nextTag(); //process <motherName>
			if (!isEmptyTag(DatabaseHelper.Individual.COLUMN_MOTHER_NAME, parser)) {
				parser.next();
				table.setMotherName(parser.getText());
				parser.nextTag(); //process </motherName>
			}else{
				table.setMotherName("");
				parser.nextTag();
			}

			parser.nextTag(); //process <fatherId>
			if (!isEmptyTag(DatabaseHelper.Individual.COLUMN_FATHER_ID, parser)) {
				parser.next();
				table.setFatherId(parser.getText());
				parser.nextTag(); //process </fatherId>
			}else{
				table.setFatherId("");
				parser.nextTag();
			}

			parser.nextTag(); //process <fatherName>
			if (!isEmptyTag(DatabaseHelper.Individual.COLUMN_FATHER_NAME, parser)) {
				parser.next();
				table.setFatherName(parser.getText());
				parser.nextTag(); //process </fatherName>
			}else{
				table.setFatherName("");
				parser.nextTag();
			}

			parser.nextTag(); //process <householdId>
			if (!isEmptyTag(DatabaseHelper.Individual.COLUMN_HOUSEHOLD_ID, parser)) {
				parser.next();
				table.setHouseholdId(parser.getText());
				parser.nextTag(); //process </householdId>
			}else{
				table.setHouseholdId("");
				parser.nextTag();
			}

			parser.nextTag(); //process <householdNo>
			if (!isEmptyTag(DatabaseHelper.Individual.COLUMN_HOUSEHOLD_NO, parser)) {
				parser.next();
				table.setHouseholdNo(parser.getText());
				parser.nextTag(); //process </householdNo>
			}else{
				table.setHouseholdNo("");
				parser.nextTag();
			}

			parser.nextTag(); //process <gpsAccuracy>
			if (!isEmptyTag(DatabaseHelper.Individual.COLUMN_GPS_ACCURACY, parser)) {
				parser.next();
				if (StringUtil.isDouble(parser.getText())){
					table.setGpsAccuracy(Double.parseDouble(parser.getText()));
					parser.nextTag(); //process </gpsAccuracy>
				}else {
					table.setGpsAccuracy(null);
					table.setGpsAvailable(false);
				}
				//Log.d("note gpsacc", table.getGpsAccuracy());
			}else{
				table.setGpsAccuracy(null);
				table.setGpsAvailable(false);
				parser.nextTag();

			}

			parser.nextTag(); //process <gpsAltitude>
			if (!isEmptyTag(DatabaseHelper.Individual.COLUMN_GPS_ALTITUDE, parser)) {
				parser.next();
				if (StringUtil.isDouble(parser.getText())){
					table.setGpsAltitude(Double.parseDouble(parser.getText()));
					parser.nextTag(); //process </gpsAltitude>
				} else {
					table.setGpsAltitude(null);
					table.setGpsAvailable(false);
				}
			}else{
				table.setGpsAltitude(null);
				table.setGpsAvailable(false);
				parser.nextTag();
			}

			parser.nextTag(); //process <gpsLatitude>
			if (!isEmptyTag(DatabaseHelper.Individual.COLUMN_GPS_LATITUDE, parser)) {
				parser.next();
				if (StringUtil.isDouble(parser.getText())) {
					table.setGpsLatitude(Double.parseDouble(parser.getText()));
					table.setCosLatitude(Math.cos(table.getGpsLatitude() * Math.PI / 180.0)); // cos_lat = cos(lat * PI / 180)
					table.setSinLatitude(Math.sin(table.getGpsLatitude() * Math.PI / 180.0)); // sin_lat = sin(lat * PI / 180)
					parser.nextTag(); //process </gpsLatitude>
				} else {
					table.setGpsLatitude(null);
					table.setGpsAvailable(false);
				}
			}else{
				table.setGpsLatitude(null);
				table.setGpsAvailable(false);
				parser.nextTag();
			}

			parser.nextTag(); //process <gpsLongitude>
			if (!isEmptyTag(DatabaseHelper.Individual.COLUMN_GPS_LONGITUDE, parser)) {
				parser.next();
				if (StringUtil.isDouble(parser.getText())) {
					table.setGpsLongitude(Double.parseDouble(parser.getText()));
					table.setCosLongitude(Math.cos(table.getGpsLongitude() * Math.PI / 180.0)); // cos_lng = cos(lng * PI / 180)
					table.setSinLongitude(Math.sin(table.getGpsLongitude() * Math.PI / 180.0)); // sin_lng = sin(lng * PI / 180)
					parser.nextTag(); //process </gpsLongitude>
				} else {
					table.setGpsLongitude(null);
					table.setGpsAvailable(false);
				}
			}else{
				table.setGpsLongitude(null);
				table.setGpsAvailable(false);
				parser.nextTag();
			}


			parser.nextTag(); //process last tag
			parser.next();

			//values.add(table);
			database.insert(table);

			if (count % 200 == 0){
				publishProgress(count);
			}

		}

		publishProgress(count);

		database.setTransactionSuccessful();
		database.endTransaction();
		database.close();

		updateSyncReport(SyncReport.REPORT_INDIVIDUALS, new Date(), SyncReport.STATUS_SYNCED);
	}

	private void processVaControlsParams(XmlPullParser parser) throws XmlPullParserException, IOException {

		//clear sync_report
		updateSyncReport(SyncReport.REPORT_VA_CONTROLS, null, SyncReport.STATUS_NOT_SYNCED);

		int count = 0;
		values.clear();

		parser.nextTag();

		Database database = getDatabase();
		database.open();
		database.beginTransaction();

		while (notEndOfXmlDoc("vacontrols", parser)) {
			count++;

			VerbalAutopsyControl table = new VerbalAutopsyControl();

			parser.nextTag(); //process <extId>
			if (!isEmptyTag(DatabaseHelper.VerbalAutopsyControl.COLUMN_EXT_ID, parser)) {
				parser.next();
				table.setExtId(parser.getText()); Log.d("extId",""+table.getExtId() );
				parser.nextTag(); //process </extId>
			}else{
				table.setExtId("");
				parser.nextTag();
			}

			parser.nextTag(); //process <code>
			if (!isEmptyTag(DatabaseHelper.VerbalAutopsyControl.COLUMN_CODE, parser)) {
				parser.next();
				table.setCode(parser.getText());
				parser.nextTag(); //process </code>
			}else{
				table.setCode("");
				parser.nextTag();
			}

			parser.nextTag(); //process <name>
			if (!isEmptyTag(DatabaseHelper.VerbalAutopsyControl.COLUMN_NAME, parser)) {
				parser.next();
				table.setName(parser.getText());
				parser.nextTag(); //process </name>
			}else{
				table.setName("");
				parser.nextTag();
			}

			parser.nextTag(); //process <gender>
			if (!isEmptyTag(DatabaseHelper.VerbalAutopsyControl.COLUMN_GENDER, parser)) {
				parser.next();
				table.setGender(parser.getText());
				parser.nextTag(); //process </gender>
			}else{
				table.setGender("");
				parser.nextTag();
			}

			parser.nextTag(); //process <dob>
			if (!isEmptyTag(DatabaseHelper.VerbalAutopsyControl.COLUMN_DATE_OF_BIRTH, parser)) {
				parser.next();
				table.setDateOfBirth(parser.getText());
				parser.nextTag(); //process </dob>
			}else{
				table.setDateOfBirth("");
				parser.nextTag();
			}

			parser.nextTag(); //process <ddth>
			if (!isEmptyTag(DatabaseHelper.VerbalAutopsyControl.COLUMN_DATE_OF_DEATH, parser)) {
				parser.next();
				table.setDateOfDeath(parser.getText());
				parser.nextTag(); //process </ddth>
			}else{
				table.setDateOfDeath("");
				parser.nextTag();
			}

			parser.nextTag(); //process <ageAtDeath>
			if (!isEmptyTag(DatabaseHelper.VerbalAutopsyControl.COLUMN_AGE_AT_DEATH, parser)) {
				parser.next();
				table.setAgeAtDeath(Integer.parseInt(parser.getText()));
				parser.nextTag(); //process </ageAtDeath>
			}else{
				table.setAgeAtDeath(-1);
				parser.nextTag();
			}

			//----

			parser.nextTag(); //process <motherId>
			if (!isEmptyTag(DatabaseHelper.VerbalAutopsyControl.COLUMN_MOTHER_ID, parser)) {
				parser.next();
				table.setMotherId(parser.getText());
				parser.nextTag(); //process </motherId>
			}else{
				table.setMotherId("");
				parser.nextTag();
			}

			parser.nextTag(); //process <motherName>
			if (!isEmptyTag(DatabaseHelper.VerbalAutopsyControl.COLUMN_MOTHER_NAME, parser)) {
				parser.next();
				table.setMotherName(parser.getText());
				parser.nextTag(); //process </motherName>
			}else{
				table.setMotherName("");
				parser.nextTag();
			}

			parser.nextTag(); //process <fatherId>
			if (!isEmptyTag(DatabaseHelper.VerbalAutopsyControl.COLUMN_FATHER_ID, parser)) {
				parser.next();
				table.setFatherId(parser.getText());
				parser.nextTag(); //process </fatherId>
			}else{
				table.setFatherId("");
				parser.nextTag();
			}

			parser.nextTag(); //process <fatherName>
			if (!isEmptyTag(DatabaseHelper.VerbalAutopsyControl.COLUMN_FATHER_NAME, parser)) {
				parser.next();
				table.setFatherName(parser.getText());
				parser.nextTag(); //process </fatherName>
			}else{
				table.setFatherName("");
				parser.nextTag();
			}

			//----

			parser.nextTag(); //process <householdId>
			if (!isEmptyTag(DatabaseHelper.VerbalAutopsyControl.COLUMN_HOUSEHOLD_ID, parser)) {
				parser.next();
				table.setHouseholdId(parser.getText());
				parser.nextTag(); //process </householdId>
			}else{
				table.setHouseholdId("");
				parser.nextTag();
			}

			parser.nextTag(); //process <householdNo>
			if (!isEmptyTag(DatabaseHelper.VerbalAutopsyControl.COLUMN_HOUSEHOLD_NO, parser)) {
				parser.next();
				table.setHouseholdNo(parser.getText());
				parser.nextTag(); //process </householdNo>
			}else{
				table.setHouseholdNo("");
				parser.nextTag();
			}

			//----

			parser.nextTag(); //process <gpsAccuracy>
			if (!isEmptyTag(DatabaseHelper.VerbalAutopsyControl.COLUMN_GPS_ACCURACY, parser)) {
				parser.next();
				table.setGpsAccuracy(Double.parseDouble(parser.getText()));
				parser.nextTag(); //process </gpsAccuracy>
				//Log.d("note gpsacc", table.getGpsAccuracy());
			}else{
				table.setGpsAccuracy(null);
				table.setGpsAvailable(false);
				parser.nextTag();
			}

			parser.nextTag(); //process <gpsAltitude>
			if (!isEmptyTag(DatabaseHelper.VerbalAutopsyControl.COLUMN_GPS_ALTITUDE, parser)) {
				parser.next();
				table.setGpsAltitude(Double.parseDouble(parser.getText()));
				parser.nextTag(); //process </gpsAltitude>
			}else{
				table.setGpsAltitude(null);
				table.setGpsAvailable(false);
				parser.nextTag();
			}

			parser.nextTag(); //process <gpsLatitude>
			if (!isEmptyTag(DatabaseHelper.VerbalAutopsyControl.COLUMN_GPS_LATITUDE, parser)) {
				parser.next();
				table.setGpsLatitude(Double.parseDouble(parser.getText()));
				table.setCosLatitude(Math.cos(table.getGpsLatitude()*Math.PI / 180.0)); // cos_lat = cos(lat * PI / 180)
				table.setSinLatitude(Math.sin(table.getGpsLatitude()*Math.PI / 180.0)); // sin_lat = sin(lat * PI / 180)
				parser.nextTag(); //process </gpsLatitude>
			}else{
				table.setGpsLatitude(null);
				table.setGpsAvailable(false);
				parser.nextTag();
			}

			parser.nextTag(); //process <gpsLongitude>
			if (!isEmptyTag(DatabaseHelper.VerbalAutopsyControl.COLUMN_GPS_LONGITUDE, parser)) {
				parser.next();
				table.setGpsLongitude(Double.parseDouble(parser.getText()));
				table.setCosLongitude(Math.cos(table.getGpsLongitude()*Math.PI / 180.0)); // cos_lng = cos(lng * PI / 180)
				table.setSinLongitude(Math.sin(table.getGpsLongitude()*Math.PI / 180.0)); // sin_lng = sin(lng * PI / 180)
				parser.nextTag(); //process </gpsLongitude>
			}else{
				table.setGpsLongitude(null);
				table.setGpsAvailable(false);
				parser.nextTag();
			}

			//----

			parser.nextTag(); //process <vaType>
			if (!isEmptyTag(DatabaseHelper.VerbalAutopsyControl.COLUMN_VA_TYPE, parser)) {
				parser.next();
				table.setVaType(Integer.parseInt(parser.getText()));
				parser.nextTag(); //process </vaType>
			}else{
				table.setVaType(0);
				parser.nextTag();
			}

			parser.nextTag(); //process <vaCollected>
			if (!isEmptyTag(DatabaseHelper.VerbalAutopsyControl.COLUMN_VA_COLLECTED, parser)) {
				parser.next();
				table.setVaCollected(Integer.parseInt(parser.getText()));
				parser.nextTag(); //process </vaCollected>
			}else{
				table.setVaCollected(0);
				parser.nextTag();
			}

			parser.nextTag(); //process <vaProcessedWith>
			if (!isEmptyTag(DatabaseHelper.VerbalAutopsyControl.COLUMN_VA_PROCESSED_WITH, parser)) {
				parser.next();
				table.setVaProcessedWith(parser.getText());
				parser.nextTag(); //process </vaProcessedWith>
			}else{
				table.setVaProcessedWith("");
				parser.nextTag();
			}

			parser.nextTag(); //process <vaProcessed>
			if (!isEmptyTag(DatabaseHelper.VerbalAutopsyControl.COLUMN_VA_PROCESSED, parser)) {
				parser.next();
				table.setVaProcessed(Integer.parseInt(parser.getText()));
				parser.nextTag(); //process </vaProcessed>
			}else{
				table.setVaProcessed(0);
				parser.nextTag();
			}

			parser.nextTag(); //process <vaResult>
			if (!isEmptyTag(DatabaseHelper.VerbalAutopsyControl.COLUMN_VA_RESULT, parser)) {
				parser.next();
				table.setVaResult(parser.getText());
				parser.nextTag(); //process </vaResult>
			}else{
				table.setVaResult("");
				parser.nextTag();
			}

			parser.nextTag(); //process <vaAttempts>
			if (!isEmptyTag(DatabaseHelper.VerbalAutopsyControl.COLUMN_VA_ATTEMPTS, parser)) {
				parser.next();
				table.setVaAttempts(Integer.parseInt(parser.getText()));
				parser.nextTag(); //process </vaAttempts>
			}else{
				table.setVaAttempts(0);
				parser.nextTag();
			}

			//----

			parser.nextTag(); //process <lastAttemptDate>
			if (!isEmptyTag(DatabaseHelper.VerbalAutopsyControl.COLUMN_LAST_ATTEMPT_DATE, parser)) {
				parser.next();
				table.setLastAttemptDate(parser.getText());
				parser.nextTag(); //process </lastAttemptDate>
			}else{
				table.setLastAttemptDate("");
				parser.nextTag();
			}

			parser.nextTag(); //process <lastAttemptReason>
			if (!isEmptyTag(DatabaseHelper.VerbalAutopsyControl.COLUMN_LAST_ATTEMPT_REASON, parser)) {
				parser.next();
				table.setLastAttemptReason(parser.getText());
				parser.nextTag(); //process </lastAttemptReason>
			}else{
				table.setLastAttemptReason("");
				parser.nextTag();
			}

			parser.nextTag(); //last process tag
			parser.next();

			//values.add(table);

			database.insert(table);
			Log.d("count",""+count);

			if (count % 100 == 0){
				publishProgress(count);
			}


		}

		publishProgress(count);

		database.setTransactionSuccessful();
		database.endTransaction();
		database.close();

		updateSyncReport(SyncReport.REPORT_VA_CONTROLS, new Date(), SyncReport.STATUS_SYNCED);
	}

	private void processVaStatsParams(XmlPullParser parser) throws XmlPullParserException, IOException {

		//clear sync_report
		updateSyncReport(SyncReport.REPORT_VA_STATS, null, SyncReport.STATUS_NOT_SYNCED);

		int count = 0;
		values.clear();

		Database database = getDatabase();
		database.open();
		database.beginTransaction();

		parser.nextTag(); //<form>

		while (notEndOfXmlDoc("vastats", parser)) {
			count++;

			VerbalAutopsyStats table = new VerbalAutopsyStats();

			parser.nextTag(); //process <creationDate>
			if (!isEmptyTag(DatabaseHelper.VerbalAutopsyStats.COLUMN_CREATION_DATE, parser)) {
				parser.next();
				table.setCreationDate(parser.getText());
				parser.nextTag(); //process </creationDate>
			}else{
				table.setCreationDate("");
				parser.nextTag();
			}

			parser.nextTag(); //process <totalDeaths>
			if (!isEmptyTag(DatabaseHelper.VerbalAutopsyStats.COLUMN_TOTAL_DEATHS, parser)) {
				parser.next();
				table.setTotalDeaths(Integer.parseInt(parser.getText()));
				parser.nextTag(); //process </totalDeaths>
			}else{
				table.setTotalDeaths(0);
				parser.nextTag();
			}

			parser.nextTag(); //process <totalToCollect>
			if (!isEmptyTag(DatabaseHelper.VerbalAutopsyStats.COLUMN_TOTAL_TO_COLLECT, parser)) {
				parser.next();
				table.setTotalToCollect(Integer.parseInt(parser.getText()));
				parser.nextTag(); //process </totalToCollect>
			}else{
				table.setTotalToCollect(0);
				parser.nextTag();
			}

			parser.nextTag(); //process <totalCollected>
			if (!isEmptyTag(DatabaseHelper.VerbalAutopsyStats.COLUMN_TOTAL_COLLECTED, parser)) {
				parser.next();
				table.setTotalCollected(Integer.parseInt(parser.getText()));
				parser.nextTag(); //process </totalCollected>
			}else{
				table.setTotalCollected(0);
				parser.nextTag();
			}

			parser.nextTag(); //process <totalAttempts>
			if (!isEmptyTag(DatabaseHelper.VerbalAutopsyStats.COLUMN_TOTAL_ATTEMPTS, parser)) {
				parser.next();
				table.setTotalAttempts(Integer.parseInt(parser.getText()));
				parser.nextTag(); //process </totalAttempts>
			}else{
				table.setTotalAttempts(0);
				parser.nextTag();
			}

			parser.nextTag(); //process <totalAttemptNi>
			if (!isEmptyTag(DatabaseHelper.VerbalAutopsyStats.COLUMN_TOTAL_ATTEMPT_NI, parser)) {
				parser.next();
				table.setTotalAttemptNi(Integer.parseInt(parser.getText()));
				parser.nextTag(); //process </totalAttemptNi>
			}else{
				table.setTotalAttemptNi(0);
				parser.nextTag();
			}

			parser.nextTag(); //process <totalAttemptUh>
			if (!isEmptyTag(DatabaseHelper.VerbalAutopsyStats.COLUMN_TOTAL_ATTEMPT_UH, parser)) {
				parser.next();
				table.setTotalAttemptUh(Integer.parseInt(parser.getText()));
				parser.nextTag(); //process </totalAttemptUh>
			}else{
				table.setTotalAttemptUh(0);
				parser.nextTag();
			}

			parser.nextTag(); //process <totalAttemptWdr>
			if (!isEmptyTag(DatabaseHelper.VerbalAutopsyStats.COLUMN_TOTAL_ATTEMPT_WDR, parser)) {
				parser.next();
				table.setTotalAttemptWdr(Integer.parseInt(parser.getText()));
				parser.nextTag(); //process </totalAttemptWdr>
			}else{
				table.setTotalAttemptWdr(0);
				parser.nextTag();
			}

			parser.nextTag(); //process <totalAttemptOt>
			if (!isEmptyTag(DatabaseHelper.VerbalAutopsyStats.COLUMN_TOTAL_ATTEMPT_OT, parser)) {
				parser.next();
				table.setTotalAttemptOt(Integer.parseInt(parser.getText()));
				parser.nextTag(); //process </totalAttemptOt>
			}else{
				table.setTotalAttemptOt(0);
				parser.nextTag();
			}

			parser.nextTag(); //process <totalAttemptNc>
			if (!isEmptyTag(DatabaseHelper.VerbalAutopsyStats.COLUMN_TOTAL_ATTEMPT_NC, parser)) {
				parser.next();
				table.setTotalAttemptNc(Integer.parseInt(parser.getText()));
				parser.nextTag(); //process </totalAttemptNc>
			}else{
				table.setTotalAttemptNc(0);
				parser.nextTag();
			}

			parser.nextTag();
			parser.next();


			database.insert(table);
			publishProgress(count);

		}

		state = State.SAVING;
		entity = Entity.INDIVIDUALS;

		database.setTransactionSuccessful();
		database.endTransaction();
		database.close();

		updateSyncReport(SyncReport.REPORT_VA_STATS, new Date(), SyncReport.STATUS_SYNCED);
	}

	//database.query(SyncReport.class, DatabaseHelper.SyncReport.COLUMN_REPORT_ID+"=?", new String[]{}, null, null, null);
	private void updateSyncReport(int reportId, Date date, int status){
		Database database = getDatabase();
		database.open();

		ContentValues cv = new ContentValues();
		cv.put(DatabaseHelper.SyncReport.COLUMN_DATE, date==null ? "" : StringUtil.format(date, "yyyy-MM-dd HH:mm:ss"));
		cv.put(DatabaseHelper.SyncReport.COLUMN_STATUS, status);
		database.update(SyncReport.class, cv, DatabaseHelper.SyncReport.COLUMN_REPORT_ID+" = ?", new String[]{reportId+""} );

		database.close();
	}

	protected void onPostExecute(String result) {
		listener.collectionComplete(result);
		dialog.dismiss();
		Toast.makeText(mContext, result, Toast.LENGTH_LONG).show();
	}

	private class DownloadResponse {
		private InputStream inputStream;
		private String fileMimeType;
		private long fileSize;
		private String fileName;

		public DownloadResponse(InputStream is, String fileType, String exportedFileName, long fileSize){
			this.inputStream = is;
			this.fileMimeType = fileType;
			this.fileName = exportedFileName;
			this.fileSize = fileSize;
		}

		public String getFileMimeType() {
			return fileMimeType;
		}

		public InputStream getInputStream() {
			return inputStream;
		}

		public long getFileSize() {
			return fileSize;
		}

		public String getFileName() {
			return fileName;
		}

		public boolean isXmlFile(){
			return fileMimeType.equalsIgnoreCase(XML_MIME_TYPE);
		}

		public boolean isZipFile(){
			return fileMimeType.equalsIgnoreCase(ZIP_MIME_TYPE);
		}
	}
}
