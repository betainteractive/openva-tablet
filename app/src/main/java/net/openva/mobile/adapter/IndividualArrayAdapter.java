package net.openva.mobile.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import net.openva.mobile.R;
import net.openva.mobile.model.Individual;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by paul on 7/31/17.
 */
public class IndividualArrayAdapter extends ArrayAdapter<Individual> {
    private List<Individual> individuals;
    private List<Boolean> checkableItems;
    private List<Boolean> supervisedItems;
    private Context mContext;
    private int layoutResId;
    private int selectedIndex = -1;

    public IndividualArrayAdapter(Context context, List<Individual> objects){
        super(context, R.layout.individual_item, objects);

        this.individuals = new ArrayList<>();
        this.individuals.addAll(objects);
        this.mContext = context;
        this.layoutResId = R.layout.individual_item;
    }

    public IndividualArrayAdapter(Context context, List<Individual> objects, List<Boolean> checks){
        super(context, R.layout.individual_item_chk, objects);

        this.individuals = new ArrayList<>();
        this.individuals.addAll(objects);

        if (checks != null){
            this.checkableItems = new ArrayList<>();
            this.checkableItems.addAll(checks);
        }

        this.mContext = context;
        this.layoutResId = R.layout.individual_item_chk;
    }

    public IndividualArrayAdapter(Context context, List<Individual> objects, List<Boolean> checks, List<Boolean> supervisionList){
        this(context, objects, checks);

        if (supervisionList != null){
            this.supervisedItems = new ArrayList<>();
            this.supervisedItems.addAll(supervisionList);
        }
    }

    public List<Individual> getIndividuals(){
        return this.individuals;
    }

    public void setSelectedIndex(int index){
        this.selectedIndex = index;
        notifyDataSetChanged();
    }

    public int getSelectedIndex(){
        return selectedIndex;
    }

    public Individual getSelectedIndividual(){
        return (selectedIndex < 0 || selectedIndex >= individuals.size()) ? null : individuals.get(selectedIndex);
    }

    @Override
    public Individual getItem(int position) {
        return individuals.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(layoutResId, parent, false);

        ImageView iconView = (ImageView) rowView.findViewById(R.id.iconView);
        TextView txtName = (TextView) rowView.findViewById(R.id.txtIndividualItemName);
        TextView txtCode = (TextView) rowView.findViewById(R.id.txtIndividualItemCode);
        CheckBox chkVBprocessed = (CheckBox) rowView.findViewById(R.id.chkProcessed);

        Individual individual = individuals.get(position);

        txtName.setText(individual.getName());
        txtCode.setText(individual.getCode());

        if (chkVBprocessed != null && checkableItems != null){
            chkVBprocessed.setChecked(checkableItems.get(position));
        }

        if (supervisedItems != null && position < supervisedItems.size()){
            if (supervisedItems.get(position)==true){
                txtName.setTypeface(null, Typeface.BOLD);
                iconView.setImageResource(R.drawable.individual_green_chk);
            }
        }

        if (selectedIndex == position){
            int colorB = Color.parseColor("#0073C6");

            rowView.setBackgroundColor(colorB);
            txtName.setTextColor(Color.WHITE);
            txtCode.setTextColor(Color.WHITE);
        }

        return rowView;
    }
}
