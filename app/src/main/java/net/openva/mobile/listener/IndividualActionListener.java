package net.openva.mobile.listener;

import net.openva.mobile.model.Individual;
import net.openva.mobile.model.VerbalAutopsyControl;

/**
 * Created by paul on 8/8/16.
 */
public interface IndividualActionListener {
    void onIndividualSelected(Individual individual);

    void onVaControlSelected(VerbalAutopsyControl deadIndividual);
}
