package net.openva.mobile.listener;

/**
 * Created by paul on 8/8/16.
 */
public interface ActionListener {
    public void execute();
}
