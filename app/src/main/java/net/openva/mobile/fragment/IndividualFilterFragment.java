package net.openva.mobile.fragment;


import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import net.openva.mobile.R;
import net.openva.mobile.widget.NumberPicker;

/**
 * A simple {@link Fragment} subclass.
 */
public class IndividualFilterFragment extends Fragment {


    private EditText txtIndFilterName;
    private EditText txtIndFilterCode;
    private EditText txtIndFilterHouseNr;
    private CheckBox chkIndFilterGFemale;
    private CheckBox chkIndFilterGMale;
    private NumberPicker nbpIndFilterMinAge;
    private NumberPicker nbpIndFilterMaxAge;
    private CheckBox chkIndFilterIsDead;
    private CheckBox chkIndFilterIsAlive;
    private Button btIndFilterClear;
    private Button btIndFilterSearch;

    private Listener listener;
    private SearchFilter searchFilter = SearchFilter.DEATHS;

    public enum SearchFilter {
        DEATHS, RESIDENTS
    }

    public IndividualFilterFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.individual_filter, container, false);

        initialize(view);
        return view;
    }

    public void setSearchFilter(SearchFilter filter){
        this.searchFilter = filter;

        this.chkIndFilterIsAlive.setChecked( (searchFilter==SearchFilter.RESIDENTS));
        //this.chkIndFilterIsAlive.setEnabled( (searchFilter==SearchFilter.RESIDENTS));
        this.chkIndFilterIsDead.setChecked( (searchFilter==SearchFilter.DEATHS));
        //this.chkIndFilterIsDead.setEnabled( (searchFilter==SearchFilter.DEATHS));
    }

    private void initialize(View view) {
        if (getActivity() instanceof Listener){
            this.listener = (Listener) getActivity();
        }

        this.txtIndFilterName = (EditText) view.findViewById(R.id.txtIndFilterName);
        this.txtIndFilterCode = (EditText) view.findViewById(R.id.txtIndFilterCode);
        this.txtIndFilterHouseNr = (EditText) view.findViewById(R.id.txtIndFilterCurrHousenumber);
        this.chkIndFilterGFemale = (CheckBox) view.findViewById(R.id.chkIndFilterGFemale);
        this.chkIndFilterGMale = (CheckBox) view.findViewById(R.id.chkIndFilterGMale);
        this.chkIndFilterIsDead = (CheckBox) view.findViewById(R.id.chkIndIsDead);
        this.chkIndFilterIsAlive = (CheckBox) view.findViewById(R.id.chkIndIsAlive);
        this.nbpIndFilterMinAge = (NumberPicker) view.findViewById(R.id.nbpIndFilterMinAge);
        this.nbpIndFilterMaxAge = (NumberPicker) view.findViewById(R.id.nbpIndFilterMaxAge);
        this.btIndFilterClear = (Button) view.findViewById(R.id.btIndFilterClear);
        this.btIndFilterSearch = (Button) view.findViewById(R.id.btIndFilterSearch);

        this.btIndFilterClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clear();
            }
        });

        this.btIndFilterSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSearch();
            }
        });


        if (txtIndFilterCode.getText().length()>0){
            onSearch();
        }
    }

    private void onSearch() {
        String name = txtIndFilterName.getText().toString();
        String permid = txtIndFilterCode.getText().toString();
        String houseNr = txtIndFilterHouseNr.getText().toString();
        String gender = (chkIndFilterGMale.isChecked() && chkIndFilterGFemale.isChecked()) ? "" : chkIndFilterGMale.isChecked() ? "M" : chkIndFilterGFemale.isChecked() ? "F" : "";
        boolean isDead = chkIndFilterIsDead.isChecked();
        boolean isAlive = chkIndFilterIsAlive.isChecked();
        int minAge = this.nbpIndFilterMinAge.getValue();
        int maxAge = this.nbpIndFilterMaxAge.getValue();

        listener.onSearch(name, permid, houseNr, gender, minAge, maxAge, isDead, isAlive);
    }

    private void clear(){
        this.txtIndFilterName.setText("");
        this.txtIndFilterCode.setText("");
        this.txtIndFilterHouseNr.setText("");
        this.chkIndFilterGMale.setChecked(false);
        this.chkIndFilterGFemale.setChecked(false);
        this.nbpIndFilterMinAge.setValue(0);
        this.nbpIndFilterMaxAge.setValue(120);
        this.chkIndFilterIsDead.setChecked(false);
    }

    public interface Listener {
        void onSearch(String name, String code, String householdNo, String gender, Integer minAge, Integer maxAge, boolean isDead, boolean isAlive);
    }

}
