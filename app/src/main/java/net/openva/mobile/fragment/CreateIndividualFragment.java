package net.openva.mobile.fragment;


import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;

import net.openva.mobile.R;
import net.openva.mobile.database.Converter;
import net.openva.mobile.model.Individual;
import net.openva.mobile.model.VerbalAutopsyControl;

import java.util.Date;

import mz.betainteractive.utilities.GeneralUtil;
import mz.betainteractive.utilities.StringUtil;

/**
 * A simple {@link Fragment} subclass.
 */
public class CreateIndividualFragment extends DialogFragment {

    private Listener listener;

    public Listener getListener() {
        return listener;
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        if (getActivity() instanceof Listener){
            this.listener = (Listener) getActivity();
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(this.getActivity());
        LayoutInflater inflater = this.getActivity().getLayoutInflater();

        builder.setTitle(getString(R.string.individual_newmem_title_lbl));
        builder.setView(inflater.inflate(R.layout.new_individual, null));
        builder.setCancelable(false);
        builder.setPositiveButton(R.string.individual_newmem_collect_lbl, null);
        builder.setNegativeButton(R.string.bt_cancel_lbl, null);
        final AlertDialog dialogNewIndividual = builder.create();

        dialogNewIndividual.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                Button b = dialogNewIndividual.getButton(AlertDialog.BUTTON_POSITIVE);
                b.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onAddNewMemberCollect(dialogNewIndividual);
                    }
                });
            }
        });


        return dialogNewIndividual;
    }

    private void onAddNewMemberCollect(Dialog dialogNewIndividual) {
        if (dialogNewIndividual == null) return;

        EditText txtNmHouseNumber = (EditText) dialogNewIndividual.findViewById(R.id.txtNmHouseNumber);
        EditText txtNmCode = (EditText) dialogNewIndividual.findViewById(R.id.txtNmCode);
        EditText txtNmName = (EditText) dialogNewIndividual.findViewById(R.id.txtNmName);
        RadioButton rbNmMale = (RadioButton) dialogNewIndividual.findViewById(R.id.rbNmGMale);
        DatePicker dtpNmDob = (DatePicker) dialogNewIndividual.findViewById(R.id.dtpNmDob);

        String gender = rbNmMale.isChecked() ? "M" : "F";

        Individual individual = Individual.getEmptyIndividual();
        individual.setHouseholdId(txtNmHouseNumber.getText().toString());
        individual.setHouseholdNo(txtNmHouseNumber.getText().toString());
        individual.setExtId(txtNmCode.getText().toString());
        individual.setCode(txtNmCode.getText().toString());
        individual.setName(txtNmName.getText().toString());
        individual.setGender(gender);
        individual.setDateOfBirth(StringUtil.format(GeneralUtil.getDate(dtpNmDob), "yyyy-MM-dd" ));
        individual.setAge(GeneralUtil.getAge(GeneralUtil.getDate(dtpNmDob)));


        //validation
        if (individual.getHouseholdNo().isEmpty()){
            buildOkDialog(getString(R.string.individual_newmem_houseno_err_lbl));
            dialogNewIndividual.show();
            return;
        }
        if (individual.getCode().trim().isEmpty()){
            buildOkDialog(getString(R.string.individual_newmem_code_err_lbl));
            dialogNewIndividual.show();
            return;
        }
        if (individual.getName().trim().isEmpty()){
            buildOkDialog(getString(R.string.individual_newmem_name_err_lbl));
            dialogNewIndividual.show();
            return;
        }
        if (individual.getDobDate().after(new Date())){
            buildOkDialog(getString(R.string.individual_newmem_dob_err_lbl));
            dialogNewIndividual.show();
            return;
        }

        //buildOkDialog("data: "+ GeneralUtil.getDate(dtpNmDob));

        dialogNewIndividual.dismiss();

        //create va control for this individual
        VerbalAutopsyControl deadIndividual = Converter.createVerbalAutopsyControl(individual);

        //Execute listener for newly created Individual
        if (listener != null){
            listener.onIndividualCreated(deadIndividual);
        }
    }

    private void buildOkDialog(String message){
        buildOkDialog(null, message);
    }

    private void buildOkDialog(String title, String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(this.getActivity());
        title = (title==null || title.isEmpty()) ? getString(R.string.info_lbl) : title;

        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);
        builder.setPositiveButton("OK", null);
        builder.show();
    }

    public interface Listener {
        void onIndividualCreated(VerbalAutopsyControl deadIndividual);
    }
}
