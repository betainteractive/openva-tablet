package net.openva.mobile.fragment;


import android.content.ContentValues;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.util.Log;

import net.openva.mobile.R;
import net.openva.mobile.database.Database;
import net.openva.mobile.database.DatabaseHelper;
import net.openva.mobile.database.Queries;
import net.openva.mobile.model.ApplicationParam;

/**
 * A simple {@link Fragment} subclass.
 */
public class SettingsFragment extends PreferenceFragment {


    public SettingsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Load the preferences from an XML resource
        addPreferencesFromResource(R.xml.settings);

        initialize();
    }

    private void initialize() {

        EditTextPreference prefOpenVaUrl = (EditTextPreference) findPreference(ApplicationParam.OPENVA_URL);
        EditTextPreference prefOdkFormId = (EditTextPreference) findPreference(ApplicationParam.ODK_FORM_ID);
        EditTextPreference prefRedcapUrl = (EditTextPreference) findPreference(ApplicationParam.REDCAP_URL);

        String openva_url = Queries.getApplicationParamValue(ApplicationParam.OPENVA_URL, this.getActivity());
        String odkform_id = Queries.getApplicationParamValue(ApplicationParam.ODK_FORM_ID, this.getActivity());
        String redcap_url = Queries.getApplicationParamValue(ApplicationParam.REDCAP_URL, this.getActivity());

        if (openva_url != null){
            setValueInPreference(prefOpenVaUrl, openva_url);
        }
        if (odkform_id != null){
            setValueInPreference(prefOdkFormId, odkform_id);
        }
        if (redcap_url != null){
            setValueInPreference(prefRedcapUrl, redcap_url);
        }

        prefOpenVaUrl.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                //Log.d("value-changed-to", ""+newValue);
                setValueInDatabase((EditTextPreference) preference, newValue.toString());
                return true;
            }
        });

        prefOdkFormId.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                //Log.d("value-changed-to", ""+newValue);
                setValueInDatabase((EditTextPreference) preference, newValue.toString());
                return true;
            }
        });

        prefRedcapUrl.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                //Log.d("value-changed-to", ""+newValue);
                setValueInDatabase((EditTextPreference) preference, newValue.toString());
                return true;
            }
        });
    }

    private void setValueInPreference(EditTextPreference pref, String value){
        pref.setText(value);
        pref.setSummary(value);
    }

    private void setValueInDatabase(EditTextPreference pref, String newValue){
        pref.setText(newValue);
        pref.setSummary(newValue);

        updateApplicationParam(pref.getKey(), newValue);
    }

    private boolean updateApplicationParam(String name, String value){

        ContentValues cv = new ContentValues();
        cv.put(DatabaseHelper.ApplicationParam.COLUMN_VALUE, value);

        Database db = new Database(this.getActivity());
        db.open();

        int i = db.update(ApplicationParam.class, cv, DatabaseHelper.ApplicationParam.COLUMN_NAME+"=?", new String[]{ name });

        db.close();

        return i>0;
    }
}
