package net.openva.mobile.fragment;


import android.app.AlertDialog;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;

import net.openva.mobile.R;
import net.openva.mobile.adapter.IndividualArrayAdapter;
import net.openva.mobile.adapter.VaControlArrayAdapter;
import net.openva.mobile.database.Converter;
import net.openva.mobile.database.Database;
import net.openva.mobile.database.DatabaseHelper;
import net.openva.mobile.database.Queries;
import net.openva.mobile.listener.ActionListener;
import net.openva.mobile.listener.IndividualActionListener;
import net.openva.mobile.model.CollectedData;
import net.openva.mobile.model.Individual;
import net.openva.mobile.model.VerbalAutopsyControl;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class IndividualListFragment extends Fragment {

    private final String FORM_ID = "who_va_2016";

    private ListView lvIndividualsList;
    private LinearLayout listButtons;
    private List<Button> buttons = new ArrayList<>();
    /*Default buttons*/
    private Button btIndListShowIndMap;
    private Button btIndListShowClosestIndividuals;
    private Button btIndShowCollectedData;

    private View mProgressView;

    private Database database;

    private ArrayList<String> lastSearch;
    private IndividualActionListener individualActionListener;


    public enum Buttons {
        MEMBERS_MAP, CLOSEST_MEMBERS
    }

    public IndividualListFragment() {
        lastSearch = new ArrayList<>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.individual_list, container, false);

        initialize(view);
        return view;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putStringArrayList("adapter", lastSearch);
    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);

        //if (savedInstanceState != null)
        //restoreLastSearch(savedInstanceState);
    }

    public void setButtonVisibilityGone(Buttons... buttons) {

        for (Buttons button : buttons) {
            if (button == Buttons.MEMBERS_MAP) {
                btIndListShowIndMap.setVisibility(View.GONE);
            }
            if (button == Buttons.CLOSEST_MEMBERS) {
                btIndListShowClosestIndividuals.setVisibility(View.GONE);
            }
        }
    }

    public void setButtonEnabled(boolean enabled, Buttons... buttons) {

        for (Buttons button : buttons) {
            if (button == Buttons.MEMBERS_MAP) {
                btIndListShowIndMap.setEnabled(enabled);
            }
            if (button == Buttons.CLOSEST_MEMBERS) {
                btIndListShowClosestIndividuals.setEnabled(enabled);
            }
        }
    }

    public Button addButton(String buttonName, final ActionListener action) {
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.height = 80;

        Button button = new Button(this.getActivity());
        button.setText(buttonName);
        button.setLayoutParams(params);
        button.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.button_normal));

        //buttons.add(button);
        listButtons.addView(button);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                action.execute();
            }
        });

        return button;
    }

    private void restoreLastSearch(Bundle savedInstanceState) {
        /*
        ArrayList<String> list = savedInstanceState.getStringArrayList("adapter");
        if (list != null && list.size() > 0) {
            String name = list.get(1);
            String peid = list.get(2);
            String hsnr = list.get(3);
            String gndr = list.get(4);
            Integer min = Integer.getInteger(list.get(5));
            Integer max = Integer.getInteger(list.get(6));
            Boolean filter1 = list.get(7).equals("true"); //dth
            Boolean filter2 = list.get(8).equals("true"); //ext
            Boolean filter3 = list.get(9).equals("true"); //na

            Log.d("restoring", "" + name);
            IndividualArrayAdapter ma = loadIndividualsByFilters(household, name, peid, hsnr, gndr, min, max, filter1, filter2, filter3);
            setAdapter(ma);
        }
        */
    }

    private void initialize(View view) {
        if (getActivity() instanceof IndividualActionListener) {
            this.individualActionListener = (IndividualActionListener) getActivity();
        }

        this.lvIndividualsList = (ListView) view.findViewById(R.id.lvIndividualsList);
        this.btIndListShowIndMap = (Button) view.findViewById(R.id.btIndListShowIndMap);
        this.btIndListShowClosestIndividuals = (Button) view.findViewById(R.id.btIndListShowClosestIndividuals);
        this.btIndShowCollectedData = (Button) view.findViewById(R.id.btIndShowCollectedData);
        this.listButtons = (LinearLayout) view.findViewById(R.id.viewListButtons);
        this.mProgressView = view.findViewById(R.id.viewListProgressBar);

        this.btIndListShowIndMap.setEnabled(false);
        this.btIndListShowClosestIndividuals.setEnabled(false);


        this.btIndListShowIndMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //showIndividualsGpsMap();
            }
        });

        this.btIndListShowClosestIndividuals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //buildMemberDistanceSelectorDialog();
            }
        });

        this.btIndShowCollectedData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCollectedData();
            }
        });

        this.lvIndividualsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                onItemClicked(position);
            }
        });

        this.lvIndividualsList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                onItemLongClicked(position);
                return true;
            }
        });

        this.database = new Database(getActivity());
    }

    private void buildOkDialog(String message) {
        buildOkDialog(null, message);
    }

    private void buildOkDialog(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.getActivity());
        title = (title == null || title.isEmpty()) ? getString(R.string.info_lbl) : title;

        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);
        builder.setPositiveButton("OK", null);
        builder.show();
    }

    private void onItemLongClicked(int position) {
        IndividualArrayAdapter adapter1 = getIndividualAdapter();
        VaControlArrayAdapter adapter2 = getVaControlAdapter();

        if (adapter1 != null){
            adapter1.setSelectedIndex(position);
            //this.btIndListShowClosestIndividuals.setEnabled(true);
        }

        if (adapter2 != null){
            adapter2.setSelectedIndex(position);
            //this.btIndListShowClosestIndividuals.setEnabled(true);
        }
    }

    private void onItemClicked(int position) {
        IndividualArrayAdapter adapter1 = getIndividualAdapter();
        VaControlArrayAdapter adapter2 = getVaControlAdapter();

        if (adapter1 != null){
            Individual individual = adapter1.getItem(position);
            if (individualActionListener != null){
                adapter1.setSelectedIndex(-1);
                this.btIndListShowClosestIndividuals.setEnabled(false);

                individualActionListener.onIndividualSelected(individual);
            }
        }

        if (adapter2 != null){
            VerbalAutopsyControl control = adapter2.getItem(position);
            if (individualActionListener != null){
                adapter2.setSelectedIndex(-1);
                this.btIndListShowClosestIndividuals.setEnabled(false);

                individualActionListener.onVaControlSelected(control);
            }
        }
    }

    private void showCollectedData() {
        this.showProgress(true);
        CollectedDataSearchTask task = new CollectedDataSearchTask();
        task.execute();
    }

    public IndividualArrayAdapter loadIndividualsByFilters(String name, String code, String householdNo, String gender, Integer minAge, Integer maxAge) {
        //open loader

        String endType = "";

        if (name == null) name = "";
        if (code == null) code = "";
        if (householdNo == null) householdNo = "";
        if (gender == null) gender = "";

        //save last search
        this.lastSearch = new ArrayList();       
        this.lastSearch.add(name);
        this.lastSearch.add(code);
        this.lastSearch.add(householdNo);
        this.lastSearch.add(gender);
        this.lastSearch.add(minAge == null ? "" : minAge.toString());
        this.lastSearch.add(maxAge == null ? "" : maxAge.toString());


        //search on database
        List<Individual> individuals = new ArrayList<>();
        List<String> whereValues = new ArrayList<>();
        String whereClause = "";

        if (!name.isEmpty()) {
            whereClause = DatabaseHelper.Individual.COLUMN_NAME + " like ?";
            whereValues.add(name + "%");
        }
        if (!code.isEmpty()) {
            whereClause += (whereClause.isEmpty() ? "" : " AND ");
            whereClause += DatabaseHelper.Individual.COLUMN_CODE + " like ?";
            whereValues.add(code + "%");
        }
        if (!householdNo.isEmpty()) {
            whereClause += (whereClause.isEmpty() ? "" : " AND ");
            whereClause += DatabaseHelper.Individual.COLUMN_HOUSEHOLD_NO + " like ?";
            whereValues.add(householdNo + "%");
        }
        if (!gender.isEmpty()) {
            whereClause += (whereClause.isEmpty() ? "" : " AND ");
            whereClause += DatabaseHelper.Individual.COLUMN_GENDER + " = ?";
            whereValues.add(gender);
        }
        if (minAge != null) {
            whereClause += (whereClause.isEmpty() ? "" : " AND ");
            whereClause += DatabaseHelper.Individual.COLUMN_AGE + " >= ?";
            whereValues.add(minAge.toString());
        }
        if (maxAge != null) {
            whereClause += (whereClause.isEmpty() ? "" : " AND ");
            whereClause += DatabaseHelper.Individual.COLUMN_AGE + " <= ?";
            whereValues.add(maxAge.toString());
        }

        database.open();

        String[] ar = new String[whereValues.size()];
        Cursor cursor = database.query(Individual.class, DatabaseHelper.Individual.ALL_COLUMNS, whereClause, whereValues.toArray(ar), null, null, DatabaseHelper.Individual.COLUMN_CODE);

        while (cursor.moveToNext()) {
            Individual individual = Converter.cursorToIndividual(cursor);
            individuals.add(individual);
        }

        database.close();

        Log.d("running","sdsd");

        IndividualArrayAdapter currentAdapter = new IndividualArrayAdapter(this.getActivity(), individuals);

        return currentAdapter;

    }

    public VaControlArrayAdapter loadVaControlsByFilters(String name, String code, String householdNo, String gender, Integer minAge, Integer maxAge) {
        //open loader

        String endType = "";

        if (name == null) name = "";
        if (code == null) code = "";
        if (householdNo == null) householdNo = "";
        if (gender == null) gender = "";

        //save last search
        this.lastSearch = new ArrayList();
        this.lastSearch.add(name);
        this.lastSearch.add(code);
        this.lastSearch.add(householdNo);
        this.lastSearch.add(gender);
        this.lastSearch.add(minAge == null ? "" : minAge.toString());
        this.lastSearch.add(maxAge == null ? "" : maxAge.toString());


        //search on database
        List<VerbalAutopsyControl> vaControls = new ArrayList<>();
        List<String> whereValues = new ArrayList<>();
        String whereClause = "";

        if (!name.isEmpty()) {
            whereClause = DatabaseHelper.VerbalAutopsyControl.COLUMN_NAME + " like ?";
            whereValues.add(name + "%");
        }
        if (!code.isEmpty()) {
            whereClause += (whereClause.isEmpty() ? "" : " AND ");
            whereClause += DatabaseHelper.VerbalAutopsyControl.COLUMN_CODE + " like ?";
            whereValues.add(code + "%");
        }
        if (!householdNo.isEmpty()) {
            whereClause += (whereClause.isEmpty() ? "" : " AND ");
            whereClause += DatabaseHelper.VerbalAutopsyControl.COLUMN_HOUSEHOLD_NO + " like ?";
            whereValues.add(householdNo + "%");
        }
        if (!gender.isEmpty()) {
            whereClause += (whereClause.isEmpty() ? "" : " AND ");
            whereClause += DatabaseHelper.VerbalAutopsyControl.COLUMN_GENDER + " = ?";
            whereValues.add(gender);
        }
        if (minAge != null) {
            whereClause += (whereClause.isEmpty() ? "" : " AND ");
            whereClause += DatabaseHelper.VerbalAutopsyControl.COLUMN_AGE_AT_DEATH + " >= ?";
            whereValues.add(minAge.toString());
        }
        if (maxAge != null) {
            whereClause += (whereClause.isEmpty() ? "" : " AND ");
            whereClause += DatabaseHelper.VerbalAutopsyControl.COLUMN_AGE_AT_DEATH + " <= ?";
            whereValues.add(maxAge.toString());
        }

        database.open();

        String[] ar = new String[whereValues.size()];
        Cursor cursor = database.query(VerbalAutopsyControl.class, DatabaseHelper.VerbalAutopsyControl.ALL_COLUMNS, whereClause, whereValues.toArray(ar), null, null, DatabaseHelper.VerbalAutopsyControl.COLUMN_CODE);

        while (cursor.moveToNext()) {
            VerbalAutopsyControl individual = Converter.cursorToVerbalAutopsyControl(cursor);
            vaControls.add(individual);
        }

        database.close();

        VaControlArrayAdapter currentAdapter = new VaControlArrayAdapter(this.getActivity(), vaControls);

        return currentAdapter;

    }

    public void showProgress(final boolean show) {
        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        lvIndividualsList.setVisibility(show ? View.GONE : View.VISIBLE);
    }

    public void setAdapter(IndividualArrayAdapter adapter) {
        this.lvIndividualsList.setAdapter(adapter);
        //if is empty
        boolean value = (adapter == null || adapter.isEmpty());

        //disable buttons
        this.btIndListShowIndMap.setEnabled(!value);
        this.btIndListShowClosestIndividuals.setEnabled(false);

    }

    public void setAdapter(VaControlArrayAdapter adapter) {
        this.lvIndividualsList.setAdapter(adapter);
        //if is empty
        boolean value = (adapter == null || adapter.isEmpty());

        //disable buttons
        this.btIndListShowIndMap.setEnabled(!value);
        this.btIndListShowClosestIndividuals.setEnabled(false);

    }

    public ArrayAdapter getAdapter(){
        return (ArrayAdapter) lvIndividualsList.getAdapter();
    }

    public IndividualArrayAdapter getIndividualAdapter() {
        if (lvIndividualsList.getAdapter() instanceof IndividualArrayAdapter) {
            return (IndividualArrayAdapter) lvIndividualsList.getAdapter();
        }

        return null;
    }

    public VaControlArrayAdapter getVaControlAdapter() {
        if (lvIndividualsList.getAdapter() instanceof VaControlArrayAdapter) {
            return (VaControlArrayAdapter) lvIndividualsList.getAdapter();
        }

        return null;
    }

    class CollectedDataSearchTask extends AsyncTask<Void, Void, VaControlArrayAdapter> {
        private boolean withSupervision;

        public CollectedDataSearchTask(){

        }

        public CollectedDataSearchTask(boolean withSupervision){
            this.withSupervision = withSupervision;
        }

        @Override
        protected VaControlArrayAdapter doInBackground(Void... params) {
            Database db = new Database(getActivity());
            db.open();

            List<CollectedData> list = Queries.getAllCollectedDataBy(db, DatabaseHelper.CollectedData.COLUMN_FORM_ID+"=?", new String[]{ FORM_ID });
            List<VerbalAutopsyControl> verbalAutopsyControls = new ArrayList<>();
            List<Boolean> checks = new ArrayList<>();
            List<Boolean> supervList = new ArrayList<>();

            String ids = "";
            for (CollectedData cd : list){
                if (ids.length() > 0){
                    ids += ", "+cd.getRecordId();
                }else{
                    ids += ""+cd.getRecordId();
                }
            }

            Cursor cursor = db.query(VerbalAutopsyControl.class, DatabaseHelper.VerbalAutopsyControl.ALL_COLUMNS, DatabaseHelper.VerbalAutopsyControl._ID + " IN ("+ids+")", null, null, null, DatabaseHelper.VerbalAutopsyControl.COLUMN_CODE);

            while (cursor.moveToNext()){
                VerbalAutopsyControl vac = Converter.cursorToVerbalAutopsyControl(cursor);
                verbalAutopsyControls.add(vac);
                CollectedData cd = getCollectedData(list, vac);
                checks.add(cd.getFormXmlPath()!=null && !cd.getFormXmlPath().isEmpty());
                supervList.add(cd.isSupervised());
            }

            cursor.close();

            db.close();

            VaControlArrayAdapter adapter = null;

            if (withSupervision){
                adapter = new VaControlArrayAdapter(getActivity(), verbalAutopsyControls, checks, supervList);
            }else{
                adapter = new VaControlArrayAdapter(getActivity(), verbalAutopsyControls, checks);
            }

            return adapter;
        }

        public CollectedData getCollectedData(List<CollectedData> listCollecteds, VerbalAutopsyControl vaControl){
            for (CollectedData cd : listCollecteds){
                if (cd.getRecordId()==vaControl.getId()) return cd;
            }

            return null;
        }

        @Override
        protected void onPostExecute(VaControlArrayAdapter adapter) {
            setAdapter(adapter);
            showProgress(false);

            if (withSupervision){
                //btMarkAsSupervised.setEnabled(true);
            }else{
                //if (btMarkAsSupervised != null){
                //    btMarkAsSupervised.setEnabled(false);
                //}
            }
        }
    }

}
